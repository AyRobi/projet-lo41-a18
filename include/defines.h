#ifndef DEFINES_H
#define DEFINES_H
#include "config.h"

//#define DEBUG

/*	Définitions des MAX		*/
#define NB_VOITURES_MAX 1000
#define NB_PIETONS_MAX	1000

/*	Définitions taille tab	*/
#define TAILLE_TEMP_STR 			1024
#define TAILLE_COND_V 				TAILLE_TAB_VOITURE
#define TAILLE_COND_P 				TAILLE_TAB_PIETONS
#define TAILLE_TAB_PARAM_V 			(TAILLE_TAB_VOITURE / 2)
#define TAILLE_TAB_PARAM_P 			(TAILLE_TAB_PIETONS / 2)
#define TAILLE_TAB_VOITURE_TRAVERSE TAILLE_TAB_VOITURE

/*	Définitions feux		*/

#define F_ROUGE 0		/*	code pour le feu rouge	*/
#define F_ORANGE 1		/*	code pour le feu orange	*/
#define F_VERT 2		/*	code pour le feu vert	*/

#define TAILLE_TAB_FEUX 8

#define F_H_V 0			/*	position du feu haut (Axe Nord-Sud) pour les voitures dans le tableau	*/
#define F_D_V 1			/*	position du feu droit (Axe Est-Ouest) pour les voitures dans le tableau	*/
#define F_B_V 2			/*	position du feu bas (Axe Est-Ouest) pour les voitures dans le tableau	*/
#define F_G_V 3			/*	position du feu gauche (Axe Nord-Sud) pour les voitures dans le tableau	*/

#define F_H_P 4			/*	position du feu haut (Axe Nord-Sud) pour les piétons dans le tableau	*/
#define F_D_P 5			/*	position du feu droit (Axe Est-Ouest) pour les piétons dans le tableau	*/
#define F_B_P 6			/*	position du feu bas (Axe Est-Ouest) pour les piétons dans le tableau	*/
#define F_G_P 7			/*	position du feu gauche (Axe Nord-Sud) pour les piétons dans le tableau	*/


/*	Définitions voitures	*/

#define NB_FILE_V			(TAILLE_TAB_VOITURE / 2)	/*	Nombre de file de voiture convergeant vers le carrefour	*/
#define TAILLE_TAB_VOITURE	8

#define V_H 0			/*	voitures en attentes au niveau du feu du haut		*/
#define V_D 1			/*	voitures en attentes au niveau du feu à droite		*/
#define V_B 2			/*	voitures en attentes au niveau du feu du bas		*/
#define V_G 3			/*	voitures en attentes au niveau du feu à gauche		*/

#define V_H_T 4			/*	voitures en traversants au niveau du feu du haut	*/
#define V_D_T 5			/*	voitures en traversants au niveau du feu à droite	*/
#define V_B_T 6			/*	voitures en traversants au niveau du feu du bas		*/
#define V_G_T 7			/*	voitures en traversants au niveau du feu à gauche	*/

/*	Définitions piétons		*/

#define TAILLE_TAB_PIETONS 16

#define P_H_G 0			/*	nombre de piétons voulants traverser le passage piéton du haut en partant de la gauche vers la droite	*/
#define P_H_D 1			/*	nombre de piétons voulants traverser le passage piéton du haut en partant de la droite vers la gauche	*/
#define P_D_H 2			/*	nombre de piétons voulants traverser le passage piéton de droite en partant du haut vers le bas			*/
#define P_D_B 3			/*	nombre de piétons voulants traverser le passage piéton de droite en partant du bas vers le haut			*/
#define P_B_G 4			/*	nombre de piétons voulants traverser le passage piéton du bas en partant de la gauche vers la droite	*/
#define P_B_D 5			/*	nombre de piétons voulants traverser le passage piéton du bas en partant de la droite vers la gauche	*/
#define P_G_H 6			/*	nombre de piétons voulants traverser le passage piéton de gauche en partant du haut vers le bas			*/
#define P_G_B 7			/*	nombre de piétons voulants traverser le passage piéton de gauche en partant du bas vers le haut			*/



#define P_H_GD 8		/*	nombre de piétons traversants le passage piéton du haut en partant de la gauche vers la droite			*/
#define P_H_DG 9		/*	nombre de piétons traversants le passage piéton du haut en partant de la droite vers la gauche			*/
#define P_D_HB 10		/*	nombre de piétons traversants le passage piéton de droite en partant du haut vers le bas				*/
#define P_D_BH 11		/*	nombre de piétons traversants le passage piéton de droite en partant du bas vers le haut				*/
#define P_B_GD 12		/*	nombre de piétons traversants le passage piéton du bas en partant de la gauche vers la droite			*/
#define P_B_DG 13		/*	nombre de piétons traversants le passage piéton du bas en partant de la droite vers la gauche			*/
#define P_G_HB 14		/*	nombre de piétons traversants le passage piéton de gauche en partant du haut vers le bas				*/
#define P_G_BH 15		/*	nombre de piétons traversants le passage piéton de gauche en partant du bas vers le haut				*/

#endif
