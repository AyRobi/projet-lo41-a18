#ifndef PIETON_H
#define PIETON_H

/*	Includes	*/

#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include "defines.h"
#include "config.h"
#include "carrefourCommon.h"
#include "feux.h"

/*	Fonctions	*/
int reveilvoiture1(int pos);
int reveilvoiture2(int pos);
int feuxP(int pos);
int deplacementP(int pos);
void arriverP(VP_Param *param);
void attendreAuFeuP(VP_Param *param);
void traverserP(VP_Param *param);
void quitterP(VP_Param *param);
void *pieton(void *data);									/*	permet la création d'un piéton qui en bon citoyen respecte les feux		*/

#endif
