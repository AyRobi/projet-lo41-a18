#ifndef MENU_H
#define MENU_H

/*	Includes	*/
#include "config.h"
#include <stdio.h>

/*	Fonctions	*/

int affiche_menu();
int affiche_sous_menu();
void gestion_sous_menu(Config *config);
void affichage_configuration(Config *config);

#endif
