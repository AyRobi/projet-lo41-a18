#ifndef UI_H
#define UI_H

/*	defines	*/
#define MIN_R 45	/*	nombre minimum de lignes	*/
#define MIN_C 84	/*	nombre minimum de colonnes	*/

/*	FEU PIETONS	*/
#define FP_LINE_W 4	/*	taille de la ligne de feu pour un piéton	*/

/*	FEU ROUGE	*/
#define FR_PAIR (F_ROUGE + 1)
#define FR_BG COLOR_RED
#define FR_FG COLOR_WHITE

/*	FEU ORANGE	*/
#define FO_PAIR (F_ORANGE + 1)
#define FO_BG COLOR_YELLOW
#define FO_FG COLOR_WHITE

/*	FEU VERT	*/
#define FV_PAIR (F_VERT + 1)
#define FV_BG COLOR_GREEN
#define FV_FG COLOR_WHITE

/*	FENETRE GENERAL	*/
#define UI_PAIR (FV_PAIR + 1)
#define UI_BG COLOR_BLACK		/*	couleur de fond de l'application	*/
#define UI_FG COLOR_CYAN		/*	couleur du texte de l'application	*/

/*	TITRE	*/
#define UI_TITLE "GESTION D'UN CARREFOUR"		/*	titre de l'application	*/
#define UI_TITLE_SIZE strlen(UI_TITLE)
#define TITLE_CHAR '='			/*	caractère utilisé pour combler les vides	*/
#define TITLE_PAIR (UI_PAIR + 1)			/*	pair de couleur pour le titre	*/
#define TITLE_BG COLOR_WHITE	/*	couleur de fond du titre	*/
#define TITLE_FG COLOR_BLACK	/*	couleur du texte pour le titre	*/

/*	ROUTE	*/
#define ROAD_W 8				/*	largeur d'une voie	*/

/*	LIGNE ROUTE	EXTERIEUR*/
#define RLE_PAIR (TITLE_PAIR + 1)
#define RLE_BG COLOR_WHITE
#define RLE_FG COLOR_BLACK

/*	LIGNE ROUTE	INTERIEURE*/
#define RL_PAIR (RLE_PAIR + 1)
#define RL_BG COLOR_BLACK
#define RL_FG COLOR_WHITE

/* PIETONS	*/
#define WALKER_BAND_W 4			/*	largeur passage pièton					*/
#define WALKER_W 8				/*	largeur d'un piéton (NON MODIFIABLE)	*/
#define WALKER_H 8				/*	hauteur d'un piéton (NON MODIFIABLE)	*/
#define WALKER_PAIR (RL_PAIR + 1)
#define WALKER_BG COLOR_BLACK
#define WALKER_FG COLOR_WHITE

/*	VOITURE	*/
#define CAR_W 5					/*	largeur d'une voiture	*/
#define CAR_H 8					/*	longeur d'une voiture	*/
#define CAR_PAIR (WALKER_PAIR + 1)
#define CAR_BG COLOR_BLACK
#define CAR_FG COLOR_WHITE

/*	ERREUR	*/
#define ERR_MSG "Erreur :"
#define ERR_PAIR (CAR_PAIR + 1)
#define ERR_BG COLOR_RED		/*	couleur de fond du message d'erreur	*/
#define ERR_FG COLOR_WHITE		/*	couleur du texte */

/* includes */
#include <ncurses.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include "defines.h"

/*  fonctions   */
void handlerSIGWINCH(int a);				/*	utiliser pour redessiner la fenetre quand celle-ci change de taille	*/

void initUI();								/*  initialise une fenêtre  */

void endUI();								/*	désalloue la mémoire et ferme la fenêtre curses pour repasser sur un terminal classique	*/

/*	x = ligne, y = col	*/
void drawHCar(int x, int y);				/*	dessine une voiture horizontale avec le coin supérieure aux coordonnées (x, y)	*/

void drawVCar(int x, int y);				/*	dessine une voiture verticale avec le coin supérieure aux coordonnées (x, y)	*/

void unDrawHCar(int x, int y);				/*	efface une voiture horizontale avec le coin supérieure aux coordonnées (x, y)	*/

void unDrawVCar(int x, int y);				/*	efface une voiture verticale avec le coin supérieure aux coordonnées (x, y)	*/

void drawWalker(int x, int y);				/*	dessine un piéton aux coordonnées (x, y) (coin supérieur gauche)	*/

/*	dessine une ligne horizontale/verticale commencant aux coordonnées (x, y) d'une longueur len et d'une couleur définit par colorPair	*/

void drawHLine(int x, int y, int len, short colorPair);

void drawVLine(int x, int y, int len, short colorPair);


void printUI();								/*	affiche le carrefour /!\ affiche un carrefour vide il est donc neccessaire de faire d'autre print	*/

void printFeux(const int tabF[]);			/*  met à jour l'état des feux  */
void printFeux_th(const int tabF[], pthread_mutex_t *mutex);	/* version protégée par mutex	*/

void printV(const int tabV[]);				/*  met à jour le nombre de voitures    */
void printV_th(const int tabV[], pthread_mutex_t *mutex);	/* version protégée par mutex	*/

void printVNum(int x, int y, int num, bool isHorizontal);		/*	affiche le nombre des voitures (et la voiture) aux coordonnées (x, y) n'affiche rien si num = 0	*/

void printP(const int tabP[]);				/*  met à jour le nombre de pièton      */
void printP_th(const int tabP[], pthread_mutex_t *mutex);	/* version protégée par mutex	*/

void printPNum(int x, int y, int num);		/*	affiche le nombre de piétons aux coordonnées (x, y) n'affiche rien si num = 0	*/

void printTxt(const char * msg);			/*	affiche le texte contenu dans msg en bas de l'écran	*/
void printTxt_th(const char * msg, pthread_mutex_t *mutex);	/* version protégée par mutex	*/

void printErr(const char * msg);			/*	affiche l'erreur contenu dans msg en bas de l'écran */
void printErr_th(const char * msg, pthread_mutex_t *mutex);	/* version protégée par mutex	*/

#endif