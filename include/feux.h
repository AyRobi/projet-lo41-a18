#ifndef FEUX_H
#define FEUX_H

/*	Includes	*/

#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include "defines.h"
#include "config.h"
#include "carrefourCommon.h"

/*	Structure	*/

typedef struct FeuxParam FeuxParam;

struct FeuxParam
{
	int *tabF;											/*	Tableau contenant l'état des feux	*/
	int *tabV;											/*	Tableau de file des voitures		*/
	Config conf;										/*	Configuration du carrefour			*/
	pthread_mutex_t *mutexUI;							/*	Mutex interface						*/
	pthread_mutex_t *mutexTabF;							/*	Mutex protegeant les feux			*/
	pthread_mutex_t *mutexTabV;							/*	Mutex protegeant les voitures		*/
	pthread_cond_t *condV;								/*	Conditions sur les voitures			*/
	pthread_cond_t *condP;								/*	Conditions sur les piétons			*/
	void (*printFeux)(const int *, pthread_mutex_t *);	/*	Affichage thread safe des feux		*/
	void (*printTxt)(const char *, pthread_mutex_t *);	/*	Affichage du texte (thread safe)	*/
	void (*printErr)(const char *, pthread_mutex_t *);	/*	Affichage des erreurs (thread safe)	*/
};

/*	Fonctions	*/

void *feux(void *data);									/*	permet la création d'un jeu de feux tricolores pour un carrefour 4 voies		*/

void initializeFeux(FeuxParam *param);					/*	initialize le tableau des feux													*/

void changeFeuxStatus(									/*	change l'état des feux à l'état suivant (e.g rouge->vert->orange->rouge->...)	*/
	FeuxParam *param,
	bool isEndPhase										/*	isEndPhase doit valoir true si c'est une fin de phase false sinon 				*/
);

unsigned int nextTpsCycle(FeuxParam *param);			/*	Donne le prochain temps de cycle												*/

void setMask(FeuxParam *param);							/*	Pose le masque de signaux pour recevoir l'appuis sur le bouton piéton			*/

void blockMask(FeuxParam *param);						/*	Pose le masque de signaux pour ne plus recevoir l'appuis sur le bouton piéton	*/

void handlerSIGUSR2(int a);								/*	Utilisé pour recevoir l'appuis sur le bouton piéton								*/

void wakeUp(FeuxParam *param);							/*	Réveille les files de voitures/piétons endormis devant un feux rouge			*/

unsigned int formFtoP(									/*	Donne le numéro de file du piéton en fonction du feu F et de top				*/
	unsigned int F,
	bool top											/*	top = true ==> file piéton haute ou gauche en fonction des cas					*/
	);

#endif