#ifndef CONFIG_H
#define CONFIG_H

typedef struct Config Config;

/*	structure utilisée pour configurer les différents paramètres pris en charge par le carrefour			*/
struct Config {
	unsigned int tpsPhase;				/*	définit le temps d'une phase en s (rouge ou vert)					*/
	unsigned int tpsInterphase;			/*	définit le temps d'interfase en s (feu orange)					*/
	unsigned int fileCrit;				/*	définit la taille de la file de voiture critique				*/
	unsigned int tpsDemarrageV;			/*	définit le temps entre chaque voiture (en ms)					*/
	unsigned int tpsTraverseV;			/*	définit le temps de traversé d'une voiture (en ms)				*/
	unsigned int nbMaxVSurC;			/*	définit le nombre de voiture maximal dans une file du carrefour		*/
	unsigned int debitVoitureMoy;			/*	définit le debit moyen de voiture						*/
	unsigned int repartitionV[4];			/*	définit la repartition des voitures sur les branches du carrefour		*/
	unsigned int tpsAdd;				/*	définit le temps additionnel en cas de file critique 				*/
	unsigned int debitPietonMoy;			/*	définit le debit moyen des pietons 					*/
	unsigned int repartitionP[8];			/*	définit la repartition des pietons sur les branches du carrefour		*/
	unsigned int tempstraverserpieton;		/*	définit le temps de traversé d'un pieton						*/
	unsigned int probabouton;			/*	definit la probabilité qu'un pieton appui sur le bouton de son feu		*/
	
};

void chargement_config(Config *config);
void sauvegarde_config(Config *config);
#endif
