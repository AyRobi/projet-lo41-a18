#ifndef VOITURE_H
#define VOITURE_H

/*	Includes	*/

#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include "defines.h"
#include "config.h"
#include "carrefourCommon.h"

/*	Fonctions	*/

void *voiture(void *data);					/*	permet la création d'une voiture respectueuse de l'environement et ne tue pas trop de piétons		*/

void arriverV(VP_Param *param);				/*	La voiture arrive dans la file du carrefour															*/

void attendreAuFeuV(VP_Param *param);		/*	La voiture attend de pouvoir traverser																*/

void demarrerV(VP_Param *param);			/*	La voiture demarre																					*/

void traverserV(VP_Param *param);			/*	La voiture traverse le carrefour																	*/

void quitterV(VP_Param *param);				/*	La voiture quitte le carrefour																		*/

int voitureDevant(int vNum);				/*	renvois le numéro de file qui passe devant la file vNum												*/

int voitureGauche(int vNum);				/*	renvois le numéro de file qui passe à gauche la file vNum											*/

void initVoitureMem();						/*	Initialise la mémoire de la voiture																	*/

unsigned int pietonDevant(					/*	Donne le numero de file du piéton devant la voiture v												*/
	unsigned int v,
	bool top								/*	Top = true ==> file de haut en bas ou de gauche a droite											*/
);	


#endif