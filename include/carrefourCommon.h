#ifndef CAREFOUR_COMMON_H
#define CAREFOUR_COMMON_H

/*	Defines	*/
#define V_TYPE 0
#define P_TYPE 1

/*	Includes	*/
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include "defines.h"

/*	Structure	*/

typedef struct VP_Param VP_Param;

/*	Structure utilisée pour passer les données aux threads voitures et piétons	*/
struct VP_Param
{
	int *tabF;												/*	Tableau contenant l'état des feux						*/
	int *tabV;												/*	Tableau contenant le nombre de voiture dans chaque file	*/
	int *tabP;												/*	Tableau contenant le nombre de piéton dans chaque file	*/
	int *sumV;												/*	Nombre total de voitures								*/
	int *sumP;												/*	Nombre total de piétons									*/
	int emplacement;										/*	Donne la position originelle de la voiture/piéton		*/
	Config conf;											/*	Paramètre de simulation du carrefour					*/
	pthread_cond_t	*condV;									/*	Conditions lié à la voiture								*/
	pthread_cond_t	*condP;									/*	Conditions lié au piéton								*/
	pthread_mutex_t *mutexUI;								/*	Mutex protégeant l'interface							*/
	pthread_mutex_t *mutexTabF;								/*	Mutex protégeant les feux								*/
	pthread_mutex_t *mutexTabV;								/*	Mutex protégeant l'emplacement des voitures				*/
	pthread_mutex_t *mutexTabP;								/*	Mutex protégeant l'emplacement des piétons				*/
	pthread_mutex_t *mutexSum;								/*	Mutex protégeant le nombre total de voitures/piétons	*/
	pthread_t *threadFeux;									/*	Thread du feu											*/
	pthread_t *threads;										/*	Tableau contenant les autres thread voiture/pietons		*/
	void (*printFunc)(const int *, pthread_mutex_t *);		/*	Affichage des piétons ou voitures, thread safe			*/
	void (*printErr)(const char *, pthread_mutex_t *);		/*	Affichage des erreurs thread safe						*/
	void (*printTxt)(const char *, pthread_mutex_t *);		/*	Affichage classique, thread safe						*/
};

/*	Variables global	*/
int COUNT_DEAD;

pthread_t DEAD_T[NB_VOITURES_MAX + NB_PIETONS_MAX];

pthread_mutex_t mutexS;

/*	Fonctions	*/

/*	Utilisé pour mettre a jour les sommes de voitures et de piétons en utilisant printFunc	*/
void printSum(void (*printFunc)(const char *, pthread_mutex_t *), pthread_mutex_t *, int vSum, int pSum);

void handlerSIGUSR1(int a);									/*	Utilisé pour terminer les threads						*/

/*	Pose les masques de signaux pour voiture/piéton			*/
void setMaskVP(void (*printErr)(const char *, pthread_mutex_t *), pthread_mutex_t *mutexUI);

int rmThreadsFromTab(										/*	Supprime le thread courant de la liste de threads		*/
	pthread_t threads[],
	const int tabSize,
	pthread_t thisThread
);


#endif