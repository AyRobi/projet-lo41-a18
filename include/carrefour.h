#ifndef CARREFOUR_H
#define CARREFOUR_H

#define _GNU_SOURCE

/*	Includes	*/
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include "UI.h"
#include "defines.h"
#include "config.h"
#include "feux.h"
#include "voiture.h"
#include "pieton.h"
#include "carrefourCommon.h"

/*	Defines	*/
#define F 2										/*	Fréquence d'appel des fonction de génèration en Hz			*/
#define MAX_AWAKE 10							/*	Nombre maximal de fois où on envois un SIGUSR1 sans réponse	*/

/*	Structures	*/

typedef struct DynamicallyAllocated DynamicallyAllocated;

struct DynamicallyAllocated
{
	/*	Tableaux d'état	*/
	int *tabF;									/*	Tableau avec l'état des feux du carrefour					*/
	int *tabV;									/*	Tableau contenant le nombres de voitures dans chaque file	*/
	int *tabP;									/*	Tableau contenant le nombres de piéton dans chaque file		*/

	/*	Compteurs	*/
	int *vSum;
	int *pSum;

	/*	Paramètres passés au threads	*/
	FeuxParam *fParam;							/*	Paramètre transmit au feu					*/
	VP_Param *vParam;							/*	Tableau de paramètres transmit aux voitures	*/
	VP_Param *pParam;							/*	Tableau de paramètres transmit aux piétons	*/

	/*	Threads	*/
	pthread_t *fThread;							/*	Thread feu					*/
	pthread_t *vThreads;						/*	Tableau de thread voitures	*/
	pthread_t *pThreads;						/*	Tableau de thread piétons	*/

	/*	Mutexes	*/
	pthread_mutex_t *mutexUI;					/*	Mutex pour l'interface				*/
	pthread_mutex_t *mutexTabF;					/*	Mutex pour les feux					*/
	pthread_mutex_t *mutexTabP;					/*	Mutex pour les piétons				*/
	pthread_mutex_t *mutexTabV;					/*	Mutex pour les voitures				*/
	pthread_mutex_t *mutexSum;					/*	Mutex pour lescompteurs sommes		*/

	/*	Conditions	*/
	pthread_cond_t *condV;						/*	Conditions pour les voitures		*/
	pthread_cond_t *condP;						/*	Conditions pour les piétons			*/
};


/*	Fonctions	*/

int startCarrefour(Config config);						/*	démarre le carrefour avec les paramètres passé par config							*/

int genereByMoy(unsigned int moy, float f);				/*	Genère un nombre en fonction d'une moyenne et d'une frequence d'appel, -1 si erreur	*/

int genereByTaux(										/*	Genère un entier sur [0; taille[ en fonction de taux, -1 en cas d'erreur			*/
	unsigned int *taux,
	unsigned int taille
);

void handlerSIGINT(int a);								/*	utilisé pour fermé correctement l'application lors d'un CTRL+C						*/

void initializeF(int tabF[]);							/*	initialize le tableau des feux														*/

void initializeV(int tabV[]);							/*	initialize le tableau des voitures													*/

void initializeP(int tabP[]);							/*	initialize le tableau des piétons													*/

bool initMem(DynamicallyAllocated *da);					/*	Alloue la mémoire et renvois true si l'allocation réussis							*/

void freeMemCarrefour(DynamicallyAllocated *da);		/*	Libération mémoire des tableaux de feux, voitures, piétons et paramètre de feux		*/

void initMutex(DynamicallyAllocated *da);				/*	Initialisation des mutexes															*/

void destroyMutex(DynamicallyAllocated *da);			/*	Suppression des mutexes																*/

void initCond(DynamicallyAllocated *da);				/*	Initialisation des conditions														*/

void destroyCond(DynamicallyAllocated *da);				/*	Suppression des conditions															*/

void initParam(DynamicallyAllocated *da, Config config);/*	Initialisation des paramètres de threads											*/

void erreurCarrefour(									/*	utiliser pour afficher une erreur 													*/
	const char *msg,									/*	et faire les opérations necessaires avant de quiter									*/
	DynamicallyAllocated *da);

bool testAlloc(DynamicallyAllocated *da);				/*	Renvois true si l'allocation semble correcte (aucun pointeur NULL)					*/

bool setSignals();										/*	Redirige les signaux pour le carrefour												*/

bool unSetSignals();									/*	Remet les traitants par défaut des signaux critiques								*/

#endif