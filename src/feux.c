#include "feux.h"

bool appuisPieton;

void *feux(void *data){

	/*	récupération de la structure passée en paramètre	*/
	FeuxParam *param = (FeuxParam *) data;

	/*	Temps de cycle par défaut	*/
	unsigned int tpsCycle = param->conf.tpsPhase;

	/*	On initialise l'appuis du piéton à false	*/
	appuisPieton = false;

	/*	On pose les masques de signaux	*/
	setMask(param);

	/*	initialisations des feux	*/
	initializeFeux(param);

	/*	gestion des feux (boucle infinie, interrompus par le signal SIGUSR1)	*/
	while(true){
		/*	Si un piéton avait appuyé sur le bouton, on remet les masques de signaux pour prendre en compte le prochain appel	*/
		if(appuisPieton)
			setMask(param);

		/*	On change l'état des feux	*/
		changeFeuxStatus(param, true);

		/*	On reveille les voitures et les piétons	*/
		wakeUp(param);

		/*	On attend le temps d'un feu vert, interrompus par un signal du piéton	*/
		sleep(tpsCycle - param->conf.tpsInterphase);

		/*	Si le feu est interrompus par un appuis piéton on affiche la prise en compte et on bloque le bouton	*/
		if(appuisPieton){
			blockMask(param);
			param->printTxt("Appel piéton pris en compte...", param->mutexUI);
		}

		/*	On change l'état des feux	*/
		changeFeuxStatus(param, false);

		/*	On attend le temps du feu orange = tps d' interphase (même si un piéton bourine le bouton)	*/
		sleep(param->conf.tpsInterphase);

		/*	On recalcule le prochain temps de cycle	*/
		tpsCycle = nextTpsCycle(param);
    }

	/*	partie du code pas sensé être executé	*/
	pthread_exit((void *) EXIT_FAILURE);
}

void initializeFeux(FeuxParam *param){

	pthread_mutex_lock(param->mutexTabF);
	int i;

	/*	on met tous les feux pairs au rouge pour les voitures	*/
	for(i = 0; i < F_H_P; i += 2)
		param->tabF[i] = F_ROUGE;

	/*	on met tous les feux impairs au orange pour les voitures	*/
	for(i = 1; i < F_H_P; i += 2)
		param->tabF[i] = F_ORANGE;

	/*	on met tous les feux pairs au orange pour les piétons	*/
	for(i = F_H_P; i < TAILLE_TAB_FEUX; i += 2)
		param->tabF[i] = F_ORANGE;

	/*	on met tous les feux impairs au rouge pour les piétons	*/
	for(i = F_H_P + 1; i < TAILLE_TAB_FEUX; i += 2)
		param->tabF[i] = F_ROUGE;
	
	pthread_mutex_unlock(param->mutexTabF);

}

unsigned int nextTpsCycle(FeuxParam *param){
	pthread_mutex_lock(param->mutexTabV);
	pthread_mutex_lock(param->mutexTabF);

	bool estRouge = false;
	unsigned int nbFC = 0;	/*	nombre de file ayant atteint le seuil critique	*/
	unsigned int i, tpsPhase;

	for(i = 0; i < NB_FILE_V; i++){
		if(param->tabV[i] >= param->conf.fileCrit){
			nbFC++;

			if(param->tabF[i] == F_ROUGE)
				estRouge = true;
		}
	}

	/*	Si toutes les files ne sont pas surchargées et que au moins une file surchargée est au feu rouge on lui donne un tps additionnel	*/
	if(nbFC < NB_FILE_V && estRouge){
		tpsPhase = param->conf.tpsPhase + param->conf.tpsAdd;
		param->printTxt("Temps supplémentaire accordé", param->mutexUI);
	}else{
		tpsPhase = param->conf.tpsPhase;
	}

	pthread_mutex_unlock(param->mutexTabF);
	pthread_mutex_unlock(param->mutexTabV);

	return tpsPhase;
}

void changeFeuxStatus(FeuxParam *param, bool isEndPhase){
	
	pthread_mutex_lock(param->mutexTabF);

	int i;

	for(i = 0; i < TAILLE_TAB_FEUX; i++){
		
		switch (param->tabF[i])
		{
			case F_ROUGE:	/*	le feu est rouge on le passe au vert dans le cas où c'est bien une fin de phase		*/
				if(isEndPhase)
					param->tabF[i] = F_VERT;
				break;

			case F_ORANGE:	/*	le feu est orange on le passe au rouge		*/
				param->tabF[i] = F_ROUGE;
				break;

			case F_VERT:	/*	le feu est au vert on le passe à l'orange	*/
				param->tabF[i] = F_ORANGE;
				break;

			default:		/*	code de feu inconnus on met au rouge		*/
				param->tabF[i] = F_ROUGE;
				break;
		}
	}
	
	/*	On affiche l'état actuel du feu	*/	
	param->printFeux(param->tabF, param->mutexUI);

	pthread_mutex_unlock(param->mutexTabF);

}

void setMask(FeuxParam *param){
	/*	création du masque pour être sur de bloquer SIGINT et SIGWINCH mais pas SIGUSR1	et SIGUSR2*/
    sigset_t newSigSet;

	/*	on bloque SIGINT et SIGWINCH	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGINT);
	sigaddset(&newSigSet, SIGWINCH);
	if(pthread_sigmask(SIG_BLOCK, &newSigSet, NULL) == -1){
		param->printErr("SIGPROCMASK", param->mutexUI);
		pthread_exit((void *) EXIT_FAILURE);
	}

	/*	on autorise SIGUSR1 et SIGUSR2	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGUSR1);
	sigaddset(&newSigSet, SIGUSR2);
	if(pthread_sigmask(SIG_UNBLOCK, &newSigSet, NULL) == -1){
		param->printErr("SIGPROCMASK", param->mutexUI);
		pthread_exit((void *) EXIT_FAILURE);
	}
}

void blockMask(FeuxParam *param){
	/*	On bloque le bouton	*/
	/*	création du masque pour bloquer le prochain SIGUSR2	*/
    sigset_t newSigSet;

	/*	on bloque SIGUSR2	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGUSR2);
	if(pthread_sigmask(SIG_BLOCK, &newSigSet, NULL) == -1){
		param->printErr("SIGPROCMASK", param->mutexUI);
		pthread_exit((void *) EXIT_FAILURE);
	}
}

void handlerSIGUSR2(int a){
	/*	Si on a déjà traité un premier appuis on ne flood pas le système pour rien (la condition ne devrait jamais être verifiée avec la pose du masque)	*/
	if(appuisPieton)
		return;

	/*	On indique au feu qu'un appuis est à prendre en compte	*/
	appuisPieton = true;
}

void wakeUp(FeuxParam *param){

	pthread_mutex_lock(param->mutexTabF);
	
	int i;

	/*	On reveille les voitures	*/
	for(i = 0; i <= F_G_V; i++){
		if(param->tabF[i] == F_VERT)
			pthread_cond_signal(&(param->condV[i]));
	}

	/*	On reveille les piétons		*/
	for(i = F_H_P ; i <= F_G_P; i++){
		if(param->tabF[i] == F_VERT){
			pthread_cond_broadcast(&(param->condP[formFtoP(i, true)]));
			pthread_cond_broadcast(&(param->condP[formFtoP(i, false)]));
		}
	}

	pthread_mutex_unlock(param->mutexTabF);
}

unsigned int formFtoP(unsigned int F, bool top){
	unsigned int p = 0;
	if(top){
		
		switch (F)
		{
			case F_B_P:
				p = P_B_G;
				break;

			case F_D_P:
				p = P_D_H;
				break;
			
			case F_H_P:
				p = P_H_G;
				break;
			
			case F_G_P:
				p = P_G_H;
				break;

			default:
				break;
		}
	}else{
		
		switch (F)
		{
			case F_B_P:
				p = P_B_D;
				break;

			case F_D_P:
				p = P_D_B;
				break;
			
			case F_H_P:
				p = P_H_D;
				break;
			
			case F_G_P:
				p = P_G_B;
				break;

			default:
				break;
		}
	}

	return p;
}