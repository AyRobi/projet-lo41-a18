#include "UI.h"

void handlerSIGWINCH(int a){
    endwin();
    initUI();
    printUI();  /*  on redessine l'interface dès que la fenetre change de taille    */
}

void initUI(){
    initscr();  /*  initialise la fenêtre curses */

    /*  reset l'affichage   */
    clear();
    refresh();

    start_color();  /*  on active les couleurs  */

    /*  définition des couleurs */
    init_color(COLOR_BLACK, 0, 0, 0);

    /*  création des paires de couleurs */
    init_pair(UI_PAIR, UI_FG, UI_BG);
    init_pair(TITLE_PAIR, TITLE_FG, TITLE_BG);
    init_pair(RLE_PAIR, RLE_FG, RLE_BG);
    init_pair(RL_PAIR, RL_FG, RL_BG);
    init_pair(WALKER_PAIR, WALKER_FG, WALKER_BG);
    init_pair(CAR_PAIR, CAR_FG, CAR_BG);
    init_pair(FR_PAIR, FR_FG, FR_BG);
    init_pair(FV_PAIR, FV_FG, FV_BG);
    init_pair(FO_PAIR, FO_FG, FO_BG);
    init_pair(ERR_PAIR, ERR_FG, ERR_BG);

    /*  comportements par défaut */
    int row, col;
    
    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */

    if(row < MIN_R || col < MIN_C)    /*  si terminal trop petit on le redimensionne  */
        wresize(stdscr, MIN_R, MIN_C);
    
    curs_set(0);    /* cache le curseur */
    wbkgd(stdscr, COLOR_PAIR(UI_PAIR)); /*  peint le terminal   */
    attrset(A_NORMAL | COLOR_PAIR(UI_PAIR));

    refresh();
}

void endUI(){
    endwin();
}

void printUI(){
    int row, col, i;
    int tempCol, angle, firstStep;
    const int len = UI_TITLE_SIZE;
    
    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */
    firstStep = (col - len)/2;

    move(0, 0); /*  on place le curseur à l'origine */

    /*  on affiche le titre de la fenetre   */

    for(i = 0; i < firstStep; i++)  /*  premier séparateur  */
        addch(TITLE_CHAR | COLOR_PAIR(TITLE_PAIR));

    for(i = 0; i < len; i++)        /*  titre   */
        addch(UI_TITLE[i] | COLOR_PAIR(TITLE_PAIR));
    
    for(i = len + firstStep; i < col; i++)
        addch(TITLE_CHAR | COLOR_PAIR(TITLE_PAIR));


    /*  mise en place des axes  */
    angle = col / 2 - ROAD_W * 2;
    for(i = 1; i < angle; i++){  /*  ligne horizontale haut gauche + ligne centrale   */
        mvaddch(row/2 - ROAD_W, i, ' '  | COLOR_PAIR(RLE_PAIR));
        mvaddch(row / 2, i, '-' | COLOR_PAIR(RL_PAIR));
    }
    
    move(row/2 + ROAD_W, 1);
    for(i = 1; i < angle; i++)  /*  ligne horizontale bas gauche     */
        addch(' '  | COLOR_PAIR(RLE_PAIR));

    for(i = angle + 4 * ROAD_W; i < col - 1; i++){   /*  ligne horizontale haut droit + ligne centrale   */
        mvaddch(row/2 - ROAD_W, i, ' '  | COLOR_PAIR(RLE_PAIR));
        mvaddch(row / 2, i, '-' | COLOR_PAIR(RL_PAIR));
    }

    move(row/2 + ROAD_W, angle + 4 * ROAD_W);
    for(i = angle + 4 * ROAD_W; i < col - 1; i++)   /*  ligne horizonale bas droit */
        addch(' '  | COLOR_PAIR(RLE_PAIR));

    tempCol = col / 2 - 2 * ROAD_W - 2;
    angle = row / 2 - ROAD_W;
    for(i = 2; i < angle; i++){ /*  ligne verticale haut gauche + ligne centrale */
        move(i, tempCol);
        addch(' '  | COLOR_PAIR(RLE_PAIR));
        addch(' '  | COLOR_PAIR(RLE_PAIR));
        mvaddch(i, col/2, '|' | COLOR_PAIR(RL_PAIR));
    }

    for(i = angle + 2 * ROAD_W + 1; i < row - 2; i++){ /*  ligne verticale bas gauche + ligne centrale */
        move(i, tempCol);
        addch(' '  | COLOR_PAIR(RLE_PAIR));
        addch(' '  | COLOR_PAIR(RLE_PAIR));
        mvaddch(i, col/2, '|' | COLOR_PAIR(RL_PAIR));
    }

    tempCol = col / 2 + 2 * ROAD_W;
    for(i = 2; i < angle; i++){ /*  ligne verticale haut droit */
        move(i, tempCol);
        addch(' '  | COLOR_PAIR(RLE_PAIR));
        addch(' '  | COLOR_PAIR(RLE_PAIR));
    }

    for(i = angle + 2 * ROAD_W + 1; i < row - 2; i++){ /*  ligne verticale bas droit */
        move(i, tempCol);
        addch(' '  | COLOR_PAIR(RLE_PAIR));
        addch(' '  | COLOR_PAIR(RLE_PAIR));
    }

    /*  affichage des piétons   */
    drawWalker(row / 2 - WALKER_H - ROAD_W - 4, col / 2 - 2 * ROAD_W - 2 * WALKER_W);   /*  haut gauche */
    drawWalker(row / 2 + ROAD_W + 4, col / 2 - 2 * ROAD_W - 2*WALKER_W);    /*  bas gauche  */
    drawWalker(row / 2 - WALKER_H - ROAD_W - 4, col / 2 + 2 * ROAD_W + WALKER_W + 2);   /*  haut droit  */
    drawWalker(row / 2 + ROAD_W + 4, col / 2 + 2 * ROAD_W + WALKER_W + 2);  /*  bas droit   */

    refresh();  /*  on affiche les modifications    */
}

void printFeux(const int tabF[]){
#ifndef DEBUG

    int row, col;
    
    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */

    /*  Feux voiture */
    drawHLine(row / 2 - ROAD_W - WALKER_BAND_W + 1, col / 2 - 2 * ROAD_W + 1, 2 * ROAD_W - 2, tabF[F_H_V] + 1); /*  feu haut    */
    drawHLine(row / 2 + ROAD_W + WALKER_BAND_W - 1, col / 2 + 2, 2 * ROAD_W - 4, tabF[F_B_V] + 1);  /*  feu bas */
    drawVLine(row / 2 - ROAD_W + 2, col / 2 + 2 * ROAD_W + 2 * WALKER_BAND_W - 2, ROAD_W - 2, tabF[F_D_V] + 1); /* feu droit    */
    drawVLine(row / 2 - ROAD_W + 2, col / 2 + 2 * ROAD_W + 2 * WALKER_BAND_W - 3, ROAD_W - 2, tabF[F_D_V] + 1); /* feu droit    */
    drawVLine(row / 2 + 1, col / 2 - 2 * (ROAD_W + WALKER_BAND_W) + 1, ROAD_W - 2, tabF[F_G_V] + 1);    /* feu gauche   */
    drawVLine(row / 2 + 1, col / 2 - 2 * (ROAD_W + WALKER_BAND_W) + 2, ROAD_W - 2, tabF[F_G_V] + 1);    /* feu gauche   */

    /*  Feux piétons    */
    drawVLine(row / 2 - ROAD_W - FP_LINE_W - 2, col / 2 - 2 * (ROAD_W + 1), FP_LINE_W, tabF[F_H_P] + 1);    /*  feu haut    */
    drawVLine(row / 2 - ROAD_W - FP_LINE_W - 2, col / 2 - 2 * (ROAD_W + 1) + 1, FP_LINE_W, tabF[F_H_P] + 1);    /*  feu haut    */
    drawVLine(row / 2 - ROAD_W - FP_LINE_W - 2, col / 2 + 2 * ROAD_W, FP_LINE_W, tabF[F_H_P] + 1);    /*  feu haut    */
    drawVLine(row / 2 - ROAD_W - FP_LINE_W - 2, col / 2 + 2 * ROAD_W + 1, FP_LINE_W, tabF[F_H_P] + 1);    /*  feu haut    */

    drawVLine(row / 2 + ROAD_W + 3, col / 2 - 2 * (ROAD_W + 1), FP_LINE_W, tabF[F_B_P] + 1);    /*  feu bas    */
    drawVLine(row / 2 + ROAD_W + 3, col / 2 - 2 * (ROAD_W + 1) + 1, FP_LINE_W, tabF[F_B_P] + 1);    /*  feu bas    */
    drawVLine(row / 2 + ROAD_W + 3, col / 2 + 2 * ROAD_W, FP_LINE_W, tabF[F_B_P] + 1);    /*  feu bas    */
    drawVLine(row / 2 + ROAD_W + 3, col / 2 + 2 * ROAD_W + 1, FP_LINE_W, tabF[F_B_P] + 1);    /*  feu bas    */

    drawHLine(row / 2 - ROAD_W, col / 2 + 2 * ROAD_W + 5, 2 * FP_LINE_W, tabF[F_D_P] + 1);   /*  feu droit   */
    drawHLine(row / 2 + ROAD_W, col / 2 + 2 * ROAD_W + 5, 2 * FP_LINE_W, tabF[F_D_P] + 1);   /*  feu droit   */

    drawHLine(row / 2 - ROAD_W, col / 2 - 2 * (ROAD_W + FP_LINE_W) - 5, 2 * FP_LINE_W, tabF[F_G_P] + 1);   /*  feu gauche  */
    drawHLine(row / 2 + ROAD_W, col / 2 - 2 * (ROAD_W + FP_LINE_W) - 5, 2 * FP_LINE_W, tabF[F_G_P] + 1);   /*  feu gauche  */

    refresh();
#endif
}

void printFeux_th(const int tabF[], pthread_mutex_t *mutex){

    pthread_mutex_lock(mutex);  /*  on prend le mutex   */
    printFeux(tabF);
    pthread_mutex_unlock(mutex);  /*  on libère le mutex  */

}

void printV(const int tabV[]){
#ifndef DEBUG

    int row, col;
    
    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */

    /*  affichage des voitures de feux  */
    printVNum(row / 2 + ROAD_W + WALKER_BAND_W, col / 2 + ROAD_W - CAR_W, tabV[V_B], false);    /*  voiture bas   */
    printVNum(row / 2 - ROAD_W - (CAR_H + 1) - WALKER_BAND_W, col / 2 - 2 * ROAD_W + ROAD_W - CAR_W, tabV[V_H], false);    /*  voiture haut   */
    printVNum(row / 2 + (ROAD_W - CAR_W) / 2, col / 2 - 2 * (ROAD_W + (CAR_H + 1) + WALKER_BAND_W), tabV[V_G], true);    /*  voiture gauche  */
    printVNum(row / 2 - ROAD_W + (ROAD_W - CAR_W) / 2, col / 2 + 2 * (ROAD_W + WALKER_BAND_W), tabV[V_D], true);  /*  voiture droite  */

    /*  affichage des voiture de carrefour  */
    printVNum(row / 2, col / 2 + ROAD_W - CAR_W, tabV[V_B_T], false);    /*  voiture bas   */
    printVNum(row / 2 - (CAR_H + 1), col / 2 - 2 * ROAD_W + ROAD_W - CAR_W, tabV[V_H_T], false);    /*  voiture haut   */
    printVNum(row / 2 + (ROAD_W - CAR_W) / 2, col / 2 - 2 * (CAR_H + 1), tabV[V_G_T], true);    /*  voiture gauche  */
    printVNum(row / 2 - ROAD_W + (ROAD_W - CAR_W) / 2, col / 2, tabV[V_D_T], true);  /*  voiture droite  */

    refresh();
#endif
}

void printV_th(const int tabV[], pthread_mutex_t *mutex){

    pthread_mutex_lock(mutex);  /*  on prend le mutex   */
    printV(tabV);
    pthread_mutex_unlock(mutex);  /*  on libère le mutex  */

}

void printVNum(int x, int y, int num, bool isHorizontal){
    if(num){
        if(isHorizontal){
            drawHCar(x, y);
            mvprintw(x + CAR_W / 2 + 1, y + CAR_H / 2 + 2, "%3d", num);
        }else{
            drawVCar(x, y);
            mvprintw(x + CAR_W / 2 + 2, y + CAR_H / 2 - 1, "%3d", num);
        }
    }else{
        if(isHorizontal){
            unDrawHCar(x, y);
            mvprintw(x + CAR_W / 2 + 1, y + CAR_H / 2 + 2, "   ");
        }else{
            unDrawVCar(x, y);
            mvprintw(x + CAR_W / 2 + 2, y + CAR_H / 2 - 1, "   ");
        }
    }
}

void printP(const int tabP[]){
#ifndef DEBUG

    int row, col;
    
    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */

    printPNum(row / 2 - ROAD_W - WALKER_BAND_W + 2, col / 2 - 2 * (ROAD_W) - 6, tabP[P_H_G]);
    printPNum(row / 2 - ROAD_W - WALKER_BAND_W + 2, col / 2 + 2 * (ROAD_W) + 3, tabP[P_H_D]);
    printPNum(row / 2 - ROAD_W - WALKER_BAND_W + 2, col / 2 - ROAD_W - 3, tabP[P_H_GD]);
    printPNum(row / 2 - ROAD_W - WALKER_BAND_W + 2, col / 2 + ROAD_W, tabP[P_H_DG]);

    printPNum(row / 2 + ROAD_W + WALKER_BAND_W - 2, col / 2 - 2 * (ROAD_W) - 6, tabP[P_B_G]);
    printPNum(row / 2 + ROAD_W + WALKER_BAND_W - 2, col / 2 + 2 * (ROAD_W) + 3, tabP[P_B_D]);
    printPNum(row / 2 + ROAD_W + WALKER_BAND_W - 2, col / 2 - ROAD_W - 3, tabP[P_B_GD]);
    printPNum(row / 2 + ROAD_W + WALKER_BAND_W - 2, col / 2 + ROAD_W, tabP[P_B_DG]);

    printPNum(row / 2 - ROAD_W - WALKER_BAND_W + 3,  col / 2 + 2 * (ROAD_W) + 2, tabP[P_D_H]);
    printPNum(row / 2 + ROAD_W + WALKER_BAND_W - 3,  col / 2 + 2 * (ROAD_W) + 2, tabP[P_D_B]);
    printPNum(row / 2 + ROAD_W / 2,  col / 2 + 2 * (ROAD_W) + 2, tabP[P_D_BH]);
    printPNum(row / 2 - ROAD_W / 2,  col / 2 + 2 * (ROAD_W) + 2, tabP[P_D_HB]);

    printPNum(row / 2 - ROAD_W - WALKER_BAND_W + 3,  col / 2 - 2 * (ROAD_W) - 5, tabP[P_G_H]);
    printPNum(row / 2 + ROAD_W + WALKER_BAND_W - 3,  col / 2 - 2 * (ROAD_W) - 5, tabP[P_G_B]);
    printPNum(row / 2 + ROAD_W / 2,  col / 2 - 2 * (ROAD_W) - 5, tabP[P_G_BH]);
    printPNum(row / 2 - ROAD_W / 2,  col / 2 - 2 * (ROAD_W) - 5, tabP[P_G_HB]);

    refresh();
#endif
}

void printP_th(const int tabP[], pthread_mutex_t *mutex){

    pthread_mutex_lock(mutex);  /*  on prend le mutex   */
    printP(tabP);
    pthread_mutex_unlock(mutex);  /*  on libère le mutex  */

}

void printPNum(int x, int y, int num){
    if(num)
        mvprintw(x, y, "%3d", num);
    else
        mvprintw(x, y, "   ");
}

void drawVCar(int x, int y){
    int i;

    move(x, y + 1);
    for(i = 0; i < 2 * CAR_W - 1; i++)   /*  barre supérieur */
        addch('_' | COLOR_PAIR(CAR_PAIR));
    
    move(x + CAR_H, y + 1);
    for(i = 0; i < 2 * CAR_W - 1; i++)   /*  barre inférieure */
        addch('_' | COLOR_PAIR(CAR_PAIR));

    for(i = 1; i <= CAR_H; i++){
        mvaddch(x + i, y, '|' | COLOR_PAIR(CAR_PAIR));  /*  ligne gauche    */
        mvaddch(x + i, y + 2 * CAR_W, '|' | COLOR_PAIR(CAR_PAIR));  /*  ligne droite    */
    }

    refresh();
}

void drawHCar(int x, int y){
    int i;

    move(x, y + 1);
    for(i = 0; i < 2 * CAR_H - 1; i++)   /*  barre supérieur */
        addch('_' | COLOR_PAIR(CAR_PAIR));
    
    move(x + CAR_W, y + 1);
    for(i = 0; i < 2 * CAR_H - 1; i++)   /*  barre inférieure */
        addch('_' | COLOR_PAIR(CAR_PAIR));

    for(i = 1; i <= CAR_W; i++){
        mvaddch(x + i, y, '|' | COLOR_PAIR(CAR_PAIR));  /*  ligne gauche    */
        mvaddch(x + i, y + 2 * CAR_H, '|' | COLOR_PAIR(CAR_PAIR));  /*  ligne droite    */
    }

    refresh();
}

void unDrawVCar(int x, int y){
    int i;

    move(x, y + 1);
    for(i = 0; i < 2 * CAR_W - 1; i++)   /*  barre supérieur */
        addch(' ' | COLOR_PAIR(UI_PAIR));
    
    move(x + CAR_H, y + 1);
    for(i = 0; i < 2 * CAR_W - 1; i++)   /*  barre inférieure */
        addch(' ' | COLOR_PAIR(UI_PAIR));

    for(i = 1; i <= CAR_H; i++){
        mvaddch(x + i, y, ' ' | COLOR_PAIR(UI_PAIR));  /*  ligne gauche    */
        mvaddch(x + i, y + 2 * CAR_W, ' ' | COLOR_PAIR(UI_PAIR));  /*  ligne droite    */
    }

    refresh();
}

void unDrawHCar(int x, int y){
    int i;

    move(x, y + 1);
    for(i = 0; i < 2 * CAR_H - 1; i++)   /*  barre supérieur */
        addch(' ' | COLOR_PAIR(UI_PAIR));
    
    move(x + CAR_W, y + 1);
    for(i = 0; i < 2 * CAR_H - 1; i++)   /*  barre inférieure */
        addch(' ' | COLOR_PAIR(UI_PAIR));

    for(i = 1; i <= CAR_W; i++){
        mvaddch(x + i, y, ' ' | COLOR_PAIR(UI_PAIR));  /*  ligne gauche    */
        mvaddch(x + i, y + 2 * CAR_H, ' ' | COLOR_PAIR(UI_PAIR));  /*  ligne droite    */
    }

    refresh();
}

void drawWalker(int x, int y){
    mvaddch(x, y + 2, '_' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 1, y + 1, '/' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 1, y + 3, '\\' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 2, y + 1, '\\' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 2, y + 2, '_' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 2, y + 3, '/' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 3, y + 2, '|' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 4, y + 1, '/' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 4, y + 2, '|' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 4, y + 3, '\\' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 5, y + 2, '|' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 6, y + 1, '/' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 6, y + 3, '\\' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 7, y, '/' | COLOR_PAIR(WALKER_PAIR));
    mvaddch(x + 7, y + 4, '\\' | COLOR_PAIR(WALKER_PAIR));

    /*  actualisation de l'affichage    */
    refresh();
}

void drawHLine(int x, int y, int len, short colorPair){
    int i;

    move(x, y);
    for(i = 0; i < len; i++)
        addch(' ' | COLOR_PAIR(colorPair));
}

void drawVLine(int x, int y, int len, short colorPair){
    int i;

    for(i = 0; i < len; i++)
        mvaddch(x + i, y, ' ' | COLOR_PAIR(colorPair));
}


void printTxt(const char * msg){
#ifndef DEBUG

    int row, col;
    col = 0;
    
    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */
    move(row - 2, 0);
    clrtobot();    /*  supprime les lignes du curseur à la fin */
    refresh();

    addstr(msg);

    refresh();
#endif
}

void printTxt_th(const char * msg, pthread_mutex_t *mutex){

    pthread_mutex_lock(mutex);  /*  on prend le mutex   */
    printTxt(msg);
    pthread_mutex_unlock(mutex);  /*  on libère le mutex  */

}

void printErr(const char * msg){
#ifndef DEBUG

    int row, col;
    int errnum = errno;
    char *errstring;

    /*  on recupère l' erreur (comme un perror) */
    errstring = strerror(errnum);

    getmaxyx(stdscr, row, col); /*  recupere les dimensions de la fenetre   */
    move(row - 2, 0);
    clrtobot();    /*  supprime les lignes du curseur à la fin */
    refresh();
    
    attron(COLOR_PAIR(ERR_PAIR));
    printw("%s %s %s", ERR_MSG, msg, errstring);
    attroff(COLOR_PAIR(ERR_PAIR));

    refresh();
#endif
}

void printErr_th(const char * msg, pthread_mutex_t *mutex){

    pthread_mutex_lock(mutex);  /*  on prend le mutex   */
    printErr(msg);
    pthread_mutex_unlock(mutex);  /*  on libère le mutex  */

}