#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "carrefour.h"
#include "config.h"
#include "menu.h"

void chargement_config(Config *config){

 FILE* f_conf = NULL;

/*  On crer une config par défaut   */
    config->fileCrit                = 15;
    config->nbMaxVSurC              = 2;
    config->tpsDemarrageV           = 500;
    config->tpsTraverseV            = 1000;
    config->tpsInterphase           = 5;
    config->tpsPhase                = 15;
    config->debitVoitureMoy	    = 3600;
    config->repartitionV[0]	    = 25;
    config->repartitionV[1]	    = 25;
    config->repartitionV[2]         = 25;
    config->repartitionV[3]         = 25;
    config->tpsAdd                  = 5;
    config->debitPietonMoy          = 3600;
    config->repartitionP[0]         = 12;
    config->repartitionP[1]         = 12;
    config->repartitionP[2]         = 12;
    config->repartitionP[3]         = 12;
    config->repartitionP[4]         = 13;
    config->repartitionP[5]         = 13;
    config->repartitionP[6]         = 13;
    config->repartitionP[7]         = 13;
    config->tempstraverserpieton    = 2;
    config->probabouton             = 50;

	

    f_conf = fopen("configuration.txt", "r"); 
    if(f_conf != NULL){
        fscanf(
            f_conf,
            "%u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u",
            &(config->tpsPhase),
            &(config->tpsInterphase),
            &(config->fileCrit),
            &(config->tpsDemarrageV),
            &(config->tpsTraverseV),
            &(config->nbMaxVSurC),
	    &(config->debitVoitureMoy),
	    &(config->repartitionV[0]),
	    &(config->repartitionV[1]),
	    &(config->repartitionV[2]),
	    &(config->repartitionV[3]),
	    &(config->tpsAdd),
	    &(config->debitPietonMoy),
            &(config->repartitionP[0]),
            &(config->repartitionP[1]),
	    &(config->repartitionP[2]),
	    &(config->repartitionP[3]),
            &(config->repartitionP[4]),
            &(config->repartitionP[5]),
            &(config->repartitionP[6]),
            &(config->repartitionP[7]),
            &(config->tempstraverserpieton),
	    &(config->probabouton)
        );
        fclose(f_conf);  
    }else{
        fprintf(stderr, "Erreur impossible de charger le fichier config. Config par défaut chargée\n");
    }

    /*  Dans le cas où l'interphase est plus grande que la phase on interchange */
    if(config->tpsInterphase > config->tpsPhase){
        unsigned int tmp = config->tpsPhase;
        config->tpsPhase = config->tpsInterphase;
        config->tpsInterphase = tmp;
    }
}



void sauvegarde_config(Config *config) {
    FILE* f_conf=NULL;
/*  On écrit le fichier de configuration    */
    f_conf = fopen("configuration.txt","w+");
    if(f_conf != NULL){
        fprintf(
            f_conf,
            "%u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u",
            config->tpsPhase,
            config->tpsInterphase,
            config->fileCrit,
            config->tpsDemarrageV,
            config->tpsTraverseV,
            config->nbMaxVSurC,
	    config->debitVoitureMoy,
	    config->repartitionV[0],
	    config->repartitionV[1],
	    config->repartitionV[2],
	    config->repartitionV[3],
	    config->tpsAdd,
	    config->debitPietonMoy,
            config->repartitionP[0],
            config->repartitionP[1],
	    config->repartitionP[2],
	    config->repartitionP[3],
            config->repartitionP[4],
            config->repartitionP[5],
            config->repartitionP[6],
            config->repartitionP[7],
            config->tempstraverserpieton,
	    config->probabouton
        );
        fclose(f_conf);
    }
}
