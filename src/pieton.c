#include "pieton.h"

void *pieton(void *data){
	
	/*	récupération de la structure passée en paramètre	*/
	VP_Param *param = (VP_Param *) data;
	/*	On pose les masques de signaux	*/
	setMaskVP(param->printErr, param->mutexUI);

/*	Le pieton s'insère dans la file d'attente	*/
	arriverP(param);

/*	On attend au feu	*/
	attendreAuFeuP(param);	

/*	Le pieton traverse	*/
	traverserP(param);

/*	La pieton sort du carrefour	*/
	quitterP(param);


    pthread_exit((void *) EXIT_SUCCESS);
}

void arriverP(VP_Param *param){
	int nba = 0;
	nba=rand()%100+1;
	/*	On incremente de 1 le nombre de pieton dans la file	*/
	pthread_mutex_lock(param->mutexTabP);
	param->tabP[param->emplacement] = param->tabP[param->emplacement] + 1;
	param->printFunc(param->tabP, param->mutexUI);
	pthread_mutex_unlock(param->mutexTabP);
	pthread_mutex_lock(param->mutexTabF);
	if (nba <= param->conf.probabouton && param->tabF[param->emplacement] != F_VERT){
		//Changement feux
		pthread_kill(*(param->threadFeux), SIGUSR2);
	}
	pthread_mutex_unlock(param->mutexTabF);
}

void attendreAuFeuP(VP_Param *param){
		
	pthread_mutex_lock(param->mutexTabF);

	while (param->tabF[feuxP(param->emplacement)] != F_VERT){
		pthread_cond_wait(&(param->condP[param->emplacement]), param->mutexTabF);
	}

	pthread_mutex_unlock(param->mutexTabF);
}


void traverserP(VP_Param *param){

 	pthread_mutex_lock(param->mutexTabP);
	param->tabP[param->emplacement]-- ;
	
	param->tabP[deplacementP(param->emplacement)]++;
	param->printFunc(param->tabP, param->mutexUI);
	pthread_mutex_unlock(param->mutexTabP);

	sleep(param->conf.tempstraverserpieton);

	pthread_mutex_lock(param->mutexTabP);
	param->tabP[deplacementP(param->emplacement)]--;
	param->printFunc(param->tabP, param->mutexUI);
	pthread_mutex_unlock(param->mutexTabP);
}

void quitterP(VP_Param *param){
	
	/*	Avant de quitter on met à jour les totaux et la liste de pietons	*/
	pthread_mutex_lock(param->mutexSum);

	/*	On supprime le pieton de la liste des threads	*/
	rmThreadsFromTab(param->threads, *(param->sumP), pthread_self());

	/*	On décremante le nombre de pietons total et on affiche la nouvelle valeur	*/
	*(param->sumP) = *(param->sumP) - 1;
	printSum(param->printTxt, param->mutexUI, *(param->sumV), *(param->sumP));

	pthread_mutex_unlock(param->mutexSum);
	pthread_cond_signal(&(param->condV[reveilvoiture1(param->emplacement)]));
	pthread_cond_signal(&(param->condV[reveilvoiture2(param->emplacement)]));


}

int deplacementP(int pos)
{
int pos2;

switch (pos)
	{
		case P_H_G:
			pos2 = P_H_GD;
			break;
		case P_H_D:
			pos2 = P_H_DG;
			break;
		case P_D_H:
			pos2 = P_D_HB;
			break;
		case P_D_B:
			pos2 = P_D_BH;
			break;
		case P_B_G:
			pos2 = P_B_GD;
			break;
		case P_B_D:
			pos2 = P_B_DG;
			break;
		case P_G_H:
			pos2 = P_G_HB;
			break;
		case P_G_B:
			pos2 = P_G_BH;
			break;
		default:
			pos2 = 0;
			break;
	}

	return pos2;
}

int feuxP(int pos)
{
int pos2;

switch (pos)
	{
		case P_H_G:
			pos2 = F_H_P;
			break;
		case P_H_D:
			pos2 = F_H_P;
			break;
		case P_D_H:
			pos2 = F_D_P;
			break;
		case P_D_B:
			pos2 = F_D_P;
			break;
		case P_B_G:
			pos2 = F_B_P;
			break;
		case P_B_D:
			pos2 = F_B_P;
			break;
		case P_G_H:
			pos2 = F_G_P;
			break;
		case P_G_B:
			pos2 = F_G_P;
			break;
		default:
			pos2 = 0;
			break;
	}

	return pos2;
}

int reveilvoiture1(int pos){
	int pos2 = 0;
	switch (pos)
		{
			case P_H_G :
			case P_H_D :
				pos2 = V_H;
				break;
			case P_D_H :
			case P_D_B :
				pos2 = V_D;
				break;
			case P_B_G :
		 	case P_B_D :
				pos2 = V_B;
				break;
			case P_G_H :
			case P_G_B :
				pos2 = V_G;
				break;
			default:
				break;
		}
	return(pos2);
}

int reveilvoiture2(int pos){
	int pos2 = 0;
	switch (pos)
		{
			case P_H_G :
			case P_H_D :
				pos2 = V_B_T;
				break;
			case P_D_H :
			case P_D_B :
				pos2 = V_G_T;
				break;
			case P_B_G :
			case P_B_D :
				pos2 = V_H_T;
				break;
			case P_G_H :
			case P_G_B :
				pos2 = V_D_T;
				break;
			default:
				break;
		}

	return(pos2);
}
