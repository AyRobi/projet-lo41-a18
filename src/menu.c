#include "menu.h"

int affiche_menu()
{
    int selection;
    printf("\n                                          Gestion d'un carrefour à feu\n");
    printf("                                          ____________________________\n\n\n\n");
    printf("  0. Quitter\n");
    printf("  1. Simulation du carrefour\n");
    printf("  2. Modification des options du carrefour\n");
    printf("  3. Affichage de la configuration du carrefour\n");
    printf("\n\n Entrez votre choix (0..3) : ");
    scanf("%d", &selection);
    printf("\n");
    return selection;
}

int affiche_sous_menu()
{
    int selection = 0;
    printf("\n                                          Modification des options du carrefour\n");
    printf("                                          ____________________________________\n\n\n\n");
    printf("  0. Quitter\n");
    printf("  1. Modification des temps de phases\n");
    printf("  2. Modification des temps d'interphase (feux orange)\n");
    printf("  3. Modification de la file critique\n");
    printf("  4. Modification du temps de demarrage des voitures\n");
    printf("  5. Modification du temps de traverse d'une voiture\n");
    printf("  6. Modification du nombre de voiture maximum sur une branche du carrefour\n");
    printf("  7. Modification du débit des voitures\n");
    printf("  8. Modification de la repartition des voitures\n");
    printf("  9. Modification de temps additionnel donné à un feu en cas de file critique\n");
    printf("  10. Modification du débit des pietons\n");
    printf("  11. Modification de la repartition des pietons\n");
    printf("  12. Modification du temps de traversé d'un pieton\n");
    printf("  13. Modification de la probabilité que le pieton appui sur le bouton\n");

    printf("\n\n Entrez votre choix (0..13) : ");
    scanf("%d", &selection);
    printf("\n");
    return selection;
}

void gestion_sous_menu(Config *config){
    int choix,voi=0,pie=0;
    choix = affiche_sous_menu();
    while (choix != 0){
			voi = 0;
			pie = 0;
            switch (choix)
            { 
	  case 1 : printf("	Entrez la valeur du nouveau temps de phases (en secondes)\n"); 	
            scanf("%u", &(config->tpsPhase)); 			break;
 
		case 2 : 
						do{
						printf("	Entrez la valeur du nouveau temps d'interphase (en secondes)\n");
            scanf("%u", &(config->tpsInterphase));
						if(config->tpsPhase <= config->tpsInterphase)
							printf("	Erreur temps de phase supérieur ou égale au temps de phase : opération impossible\n");
						}while(config->tpsPhase <= config->tpsInterphase);
						break;

    case 3 :  printf("	Entrez la nouvelle valeur de la file critique\n");
            scanf("%u", &(config->fileCrit)); 			break;
	
		case 4 :  printf("	Entrez la nouvelle valeur du temps de demarrage d'une voiture (en millisecondes)\n");
            scanf("%u", &(config->tpsDemarrageV)); 			break;
               
		case 5 :  printf("	Entrez la nouvelle valeur du temps de traverser d'une voiture (en millisecondes)\n");
            scanf("%u", &(config->tpsTraverseV)); 			break; 

		case 6 :  printf("	Entrez la nouvelle valeur du nombre maximum de voiture sur le carrefour\n");
            scanf("%u", &(config->nbMaxVSurC)); 		break;
		
		case 7 :  printf("	Entrez le debit moyen de voitures (en voitures/heure)\n");
            scanf("%u", &(config->debitVoitureMoy)); 			break;

		case 8 :  
		while(voi==0)
		{
			printf("	Entrez la proportion de voitures au niveau du feu du haut (en %%)");
	    scanf("%u", &(config->repartitionV[0]));
			  printf("	Entrez la proportion de voitures au niveau du feu à droite (en %%)");
	    scanf("%u", &(config->repartitionV[1]));
	    		  printf("	Entrez la proportion de voitures au niveau du feu du bas (en %%)");
	    scanf("%u", &(config->repartitionV[2]));
	    		  printf("	Entrez la proportion de voitures au niveau du feu à gauche (en %%)");
	    scanf("%u", &(config->repartitionV[3]));
			if (config->repartitionV[0] + config->repartitionV[1] + config->repartitionV[2] + config->repartitionV[3] != 100)
			printf("	Erreur : la somme des proportions ne fait pas 100 \n	Veuillez rentrer d'autres valeurs : \n");
			else
			voi=1;
		}
	   
		break;

		case 9 :  printf("	Entrez le temps additionnel du feu en cas de file critique (en secondes)\n");
            scanf("%u", &(config->tpsAdd)); 			break;
	
		case 10 :  printf("	Entrez le debit moyen de pietons (en pietons/heure)\n");
            scanf("%u", &(config->debitPietonMoy)); 			break;

		case 11 : 
		while(pie==0)
		{
			printf("	Entrez la proportion de pietons au niveau du feu du haut en partant de la gauche vers la droite (en %%)");
	    scanf("%u", &(config->repartitionP[0]));
			printf("	Entrez la proportion de pietons au niveau du feu du haut en partant de la droite vers la gauche (en %%)");
	    scanf("%u", &(config->repartitionP[1]));
			printf("	Entrez la proportion de pietons au niveau du feu droit en partant du haut vers le bas (en %%)");
	    scanf("%u", &(config->repartitionP[2]));
			printf("	Entrez la proportion de pietons au niveau du feu droit en partant du bas vers le haut (en %%)");
	    scanf("%u", &(config->repartitionP[3]));
			printf("	Entrez la proportion de pietons au niveau du feu du bas en partant de la gauche vers la droite (en %%)");
	    scanf("%u", &(config->repartitionP[4]));
			printf("	Entrez la proportion de pietons au niveau du feu du bas en partant de la droite vers la gauche (en %%)");
	    scanf("%u", &(config->repartitionP[5]));
			printf("	Entrez la proportion de pietons au niveau du feu de gauche en partant du haut vers le bas (en %%)");
	    scanf("%u", &(config->repartitionP[6]));
			printf("	Entrez la proportion de pietons au niveau du feu de gauche en partant du bas vers le haut (en %%)");
	    scanf("%u", &(config->repartitionP[7]));
			
			if (config->repartitionP[0] + config->repartitionP[1] + config->repartitionP[2] + config->repartitionP[3] + config->repartitionP[4] + config->repartitionP[5] +config->repartitionP[6] + config->repartitionP[7] != 100)
			printf("Erreur : le sommes des proportions ne fait pas 100");
			else
			pie=1;
		}

 		break;

		case 12 :  printf("	Entrez le temps de traverser des pietons (en secondes);\n");
            scanf("%u", &(config->tempstraverserpieton)); 			     			break;

		case 13 :  printf("	Entrez la probabilité que le pieton appui sur le bouton changement d'état du feu;\n");
            scanf("%u", &(config->probabouton)); 			break;

                default : printf("erreur de choix "); break;
            }
				choix = affiche_sous_menu();
    }
}

void affichage_configuration(Config *config){
           printf("	Temps de phases (en secondes) : %u \n",config->tpsPhase);
	   printf("	Temps d'interphases (en secondes) : %u \n",config->tpsInterphase);
           printf("	Valeur de la file critique : %u \n",config->fileCrit);
	   printf("	Temps de demarage d'une voiture (en millisecondes) : %u \n",config->tpsDemarrageV);
	   printf("	Temps de traversé d'une voiture (en millisecondes) : %u \n",config->tpsTraverseV);
	   printf("	Nombre de voiture maximum sur le carrefour : %u \n",config->nbMaxVSurC);
	   printf("	Debit moyen de voitures (voitures/heure) : %u\n",config->debitVoitureMoy);
	   printf("	Repartition des voitures : \n");           
	   printf("		Voitures au niveau du feu du haut (en %%) : %u\n",config->repartitionV[0]);
	   printf("		Voitures au niveau du feu à droite (en %%) : %u\n",config->repartitionV[1]);
	   printf("		Voitures au niveau du feu du bas (en %%) : %u\n",config->repartitionV[2]);
	   printf("		Voitures au niveau du feu à gauche (en %%) : %u\n",config->repartitionV[3]);
	   printf("	Temps additionnel du feu en cas de file critique (en secondes) : %u \n",config->tpsAdd); 
	   printf("	Debit moyen de pietons (pietons/heure) : %u \n",config->debitPietonMoy);
	   printf("	Repartition des pietons : \n");
	   printf("		Feu haut en partant de la gauche vers la droite (en %%) : %u\n",config->repartitionP[0]);
	   printf("		Feu haut en partant de la droite vers la gauche (en %%) : %u\n",config->repartitionP[1]);
	   printf("		Feu droit en partant du haut vers le bas (en %%) : %u\n",config->repartitionP[2]);
	   printf("		Feu droit en partant du bas vers le haut (en %%) : %u\n",config->repartitionP[3]);
	   printf("		Feu bas en partant de la gauche vers la droite (en %%) : %u\n",config->repartitionP[4]);
	   printf("		Feu bas en partant de la droite vers la gauche (en %%) : %u\n",config->repartitionP[5]);	
	   printf("		Feu gauche en partant du haut vers le bas (en %%) : %u\n",config->repartitionP[6]);
	   printf("		Feu gauche en partant du bas vers le haut (en %%) : %u\n",config->repartitionP[7]);
	   printf("	Temps de traversé d'un pieton (en secondes) : %u \n",config->tempstraverserpieton);
	   printf("	Probabilité que le pieton appui sur le bouton (entre 0 et 100) : %u \n\n",config->probabouton);

}

