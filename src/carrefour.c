#include "carrefour.h"

bool carrefourActif;	/*	permet de savoir si le carrefour est toujours actif (feux allumés + arrivé de voitures et piétons)	*/


int startCarrefour(Config config){

	/*	Compteur de boucle	*/
	int i;

	/*	Variables temporaire	*/
	int tmp, tmp2;

	/*	Création de la structure contenant nos allocations dynamiques		*/
	DynamicallyAllocated da;

	/*	Allocation de la mémoire	*/
	if(!initMem(&da)){
		fprintf(stderr, "Erreur allocation dynamique, retour de pointeur NULL\n");
		return EXIT_FAILURE;
	}

	/*	Initialisation des mutexes	*/
	initMutex(&da);

	/*	Initialisation des conditions	*/
	initCond(&da);

	/*	Initialisation mémoire voiture	*/
	initVoitureMem();

	/*	Initialisation des compteurs	*/
	*(da.vSum) = 0;
	*(da.pSum) = 0;

	/*	Initialisations des tableaux de voitures, piétons	*/
	initializeF(da.tabF);
	initializeP(da.tabP);
	initializeV(da.tabV);

	/*	Initialisation des paramètres	*/
	initParam(&da, config);

	/*	Initialisation du compteur de mort à -1	*/
	COUNT_DEAD = -1;

	/*	Redirection des signaux	*/

	if(!setSignals()){
		erreurCarrefour("SIGPROCMASK", &da);
		return EXIT_FAILURE;
	}

	/*	création du masque pour être sur de recevoir SIGINT et SIGWINCH mais pas SIGUSR1	*/
    sigset_t oldSigSet, newSigSet;

	/*	on autorise SIGINT et SIGWINCH	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGINT);
	sigaddset(&newSigSet, SIGWINCH);
	if(pthread_sigmask(SIG_UNBLOCK, &newSigSet, &oldSigSet) == -1){
		erreurCarrefour("SIGPROCMASK", &da);
		return EXIT_FAILURE;
	}

	/*	on bloque SIGUSR1 et SIGUSR2	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGUSR1);
	sigaddset(&newSigSet, SIGUSR2);
	if(pthread_sigmask(SIG_BLOCK, &newSigSet, NULL) == -1){
		erreurCarrefour("SIGPROCMASK", &da);
		return EXIT_FAILURE;
	}

	/*	Affichage du carrefour	*/
	#ifndef DEBUG
		initUI();
		printUI();
	#endif

	/*	Lancement des feux	*/
	if(pthread_create(da.fThread, NULL, feux, (void *)da.fParam) != 0){
		erreurCarrefour("PTHREAD_CREATE", &da);
		return EXIT_FAILURE;
	}

	/*	Passage de l'état du carrefour à actif	*/
	carrefourActif = true;

	/*	Création des piétons et des voitures tant que le carrefour est considérré comme actif	*/

    while(carrefourActif){
		/*	Gestion de la création des voitures et des piétons	*/
		#if F > 0
			pthread_mutex_lock(da.mutexSum);

			/*	Si on a pas atteint le nombre max de voitures on en génère aléatoirement	*/
			if(*(da.vSum) < NB_VOITURES_MAX){
				tmp = genereByMoy(config.debitVoitureMoy, F);

				if(tmp > 0){

					/*	On crée tmp voiture	*/
					for(i = 0; i < tmp; i++){
						tmp2 = genereByTaux(config.repartitionV, TAILLE_TAB_PARAM_V);

						if(tmp2 >= 0){
							pthread_create(&(da.vThreads[*(da.vSum)]), NULL, voiture, (void *) &(da.vParam[tmp2]));
							pthread_detach(da.vThreads[*(da.vSum)]);

							*(da.vSum) = *(da.vSum) + 1;
						}
					}
				}
			}

			/*	Si on a pas atteint le nombre max de piétons on en génère aléatoirement	*/
			if(*(da.pSum) < NB_PIETONS_MAX){
				tmp = genereByMoy(config.debitPietonMoy, F);

				if(tmp > 0){

					/*	On crée tmp voiture	*/
					for(i = 0; i < tmp; i++){
						tmp2 = genereByTaux(config.repartitionP, TAILLE_TAB_PARAM_P);

						if(tmp2 >= 0){
							pthread_create(&(da.pThreads[*(da.pSum)]), NULL, pieton, (void *) &(da.pParam[tmp2]));
							pthread_detach(da.pThreads[*(da.pSum)]);

							*(da.pSum) = *(da.pSum) + 1;
						}
					}
				}
			}

			/*	On affiche les nouveaux totaux	*/
			printSum(printTxt_th, da.mutexUI, *(da.vSum), *(da.pSum));
			pthread_mutex_unlock(da.mutexSum);

			/*	On dort jusqu'à la prochaine génèration	*/
			usleep(1000000 / F);
		#else
			pause();
		#endif
    }

	/*	On prend tous les verrous	*/
	pthread_mutex_lock(da.mutexTabV);
	pthread_mutex_lock(da.mutexTabP);
	pthread_mutex_lock(da.mutexTabF);
	pthread_mutex_lock(da.mutexUI);
	pthread_mutex_lock(da.mutexSum);

	pthread_mutex_lock(&mutexS);

	int awakeError = 0;

	while(COUNT_DEAD != 0 && awakeError < MAX_AWAKE && (*(da.vSum) || *(da.pSum))){
		COUNT_DEAD = 0;
		awakeError++;

		pthread_mutex_unlock(&mutexS);
		/*	On reveille les threads endormis sur une conditions	voiture	*/
		for(i = 0; i < TAILLE_COND_V; i++)
			pthread_cond_broadcast(&(da.condV[i]));

		/*	On reveille les threads endormis sur une conditions	voiture	*/
		for(i = 0; i < TAILLE_COND_P; i++)
			pthread_cond_broadcast(&(da.condP[i]));

		/*	On dort pour que tous les autres threads se reveillent	*/
		pthread_yield();	/*	on se met tout à la fin de la queue d' ordonnancement	*/
		usleep(1000);		/*	Dans le cas où yield ne fait rien						*/

		/*	On envois un signal d'arrêt aux voitures	*/
		for(i = 0; i < *(da.vSum); i++){
			pthread_kill(da.vThreads[i], SIGUSR1);
		}
			

		/*	On envois un signal d'arrêt aux piétons	*/
		for(i = 0; i < *(da.pSum); i++){
			pthread_kill(da.pThreads[i], SIGUSR1);
		}

		
		pthread_yield();	/*	on se met tout à la fin de la queue d' ordonnancement	*/
		usleep(1000);		/*	Dans le cas où yield ne fait rien						*/

		pthread_mutex_lock(&mutexS);
		/*	On supprime les threads morts	*/
		for(i = 0; i < COUNT_DEAD; i++){
			if(rmThreadsFromTab(da.vThreads, *(da.vSum), DEAD_T[i]) == -1){
				if(rmThreadsFromTab(da.pThreads, *(da.pSum), DEAD_T[i]) == 0)
					*(da.pSum) = *(da.pSum) - 1;
			}else{
				*(da.vSum) = *(da.vSum) - 1;
			}
		}

	}
	pthread_mutex_unlock(&mutexS);

	/*	On envois un signal d'arrêt au gestionnaire de feux	*/
	pthread_kill(*(da.fThread), SIGUSR1);

	/*	Attente fin des feux	*/
	if(pthread_join(*(da.fThread), NULL) != 0){
		erreurCarrefour("PTHREAD_JOIN", &da);
		return EXIT_FAILURE;
	}

	pthread_mutex_unlock(da.mutexSum);
	pthread_mutex_unlock(da.mutexUI);
	pthread_mutex_unlock(da.mutexTabF);
	pthread_mutex_unlock(da.mutexTabP);
	pthread_mutex_unlock(da.mutexTabV);

	/*	on termine l'affichage du carrefour	*/
	#ifndef DEBUG
		endUI();
	#endif
	/*	on remet SIGINT et SIGWINCH par défaut	*/
	if(!unSetSignals()){
		erreurCarrefour("SIGPROCMASK", &da);
		return EXIT_FAILURE;
	}

	/*	on rétablit l'ancien masque	*/
	if(sigprocmask(SIG_SETMASK, &oldSigSet, NULL) == -1){
		printErr("SIGPROCMASK");
		freeMemCarrefour(&da);
		return EXIT_FAILURE;
	}

	/*	Suppression des mutexes	*/
	destroyMutex(&da);

	/*	On supprime les conditions	*/
	destroyCond(&da);

	/*	on désaloue la mémoire	*/
	freeMemCarrefour(&da);

	/*	Si on a due faire MAX_AWAKE * signaux SIGUSR1 alors on quitte	*/
	if(awakeError >= MAX_AWAKE)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

int genereByMoy(unsigned int moy, float f){
	#define MUL_FAC 1000000
	#define D 0.2

	if(f == 0)
		return -1;

	float	nf = moy / (f * 3600),
		inf = nf * (1 - D),
		sup = nf * (1 + D);
	
	/*	Génération de la partie entière avec divergence de 100*D% par rapport à la moyenne	*/
	float n1 = (rand() % ((int)(sup * MUL_FAC) - (int)(inf * MUL_FAC)  + 1) + (int)(inf * MUL_FAC)) / (float) MUL_FAC;

	int n = (int)n1;

	nf = n1 - (int)n1;

	/*	On prend en compte la partie décimal	*/
	int n3 = rand() % MUL_FAC;

	if(n3 <= nf * MUL_FAC)
		n++;

	return n;
}

int genereByTaux(unsigned int *taux, unsigned int taille){
	int s, i, x;
	s = 0;

	for(i = 0; i < taille; i++)
		s += taux[i];

	if(s == 0)
		return -1;

	x = rand() % s;
	s = 0;
	i = 0;

	while(i < taille && (x < s || x >= taux[i] + s)){
		s += taux[i];
		i++;
	}

	if(i == taille)
		return -1;
	else
		return i;
}


void handlerSIGINT(int a){
    carrefourActif = false;
}

void initializeF(int tabF[]){
	int i;

	for(i = 0; i < TAILLE_TAB_FEUX; i++)
		tabF[i] = 0;
}

void initializeV(int tabV[]){
	int i;

	for(i = 0; i < TAILLE_TAB_VOITURE; i++)
		tabV[i] = 0;
}

void initializeP(int tabP[]){
	int i;

	for(i = 0; i < TAILLE_TAB_PIETONS; i++)
		tabP[i] = 0;
}

bool initMem(DynamicallyAllocated *da){
	
	/*	Tableaux d'état de feux, voitures, piétons	*/
	da->tabF = (int *) malloc(TAILLE_TAB_FEUX * sizeof(int));
	da->tabV = (int *) malloc(TAILLE_TAB_VOITURE * sizeof(int));
	da->tabP = (int *) malloc(TAILLE_TAB_PIETONS * sizeof(int));

	/*	Compteurs	*/
	da->vSum = (int *) malloc(sizeof(int));
	da->pSum = (int *) malloc(sizeof(int));

	/*	Création des paramètres de feux, voitures et piétons	*/
	da->fParam = (FeuxParam *) malloc(sizeof(FeuxParam));
	da->vParam = (VP_Param *) malloc(TAILLE_TAB_PARAM_V * sizeof(VP_Param));
	da->pParam = (VP_Param *) malloc(TAILLE_TAB_PARAM_P * sizeof(VP_Param));

	/*	Thread pour la gestion des feux	*/
	da->fThread = (pthread_t *) malloc(sizeof(pthread_t));

	/*	Threads pour les voitures et nombres de voitures	*/
	da->vThreads = (pthread_t *) malloc(NB_VOITURES_MAX * sizeof(pthread_t));

	/*	Threads pour les pietons et nombres de piétons		*/
	da->pThreads = (pthread_t *) malloc(NB_PIETONS_MAX * sizeof(pthread_t));

	/*	Mutex pour l'UI	*/
	da->mutexUI = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));

	/*	Mutex pour l'accès aux tableaux	*/
	da->mutexTabF = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));
	da->mutexTabP = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));
	da->mutexTabV = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));

	/*	Mutex pour les sommes et tableaux de threads de voitures et de piétons	*/
	da->mutexSum = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));

	/*	Conditions pour les voitures et les piétons	*/
	da->condV = (pthread_cond_t *) malloc(TAILLE_COND_V * sizeof(pthread_cond_t));
	da->condP = (pthread_cond_t *) malloc(TAILLE_COND_P * sizeof(pthread_cond_t));

	return testAlloc(da);
}

void freeMemCarrefour(DynamicallyAllocated *da){
#ifdef DEBUG
	printf("Free MEM\n");
#endif

	free(da->tabF);
	free(da->tabV);
	free(da->tabP);
	free(da->vSum);
	free(da->pSum);
	free(da->fParam);
	free(da->vParam);
	free(da->pParam);
	free(da->fThread);
	free(da->vThreads);
	free(da->pThreads);
	free(da->mutexUI);
	free(da->mutexTabF);
	free(da->mutexTabP);
	free(da->mutexTabV);
	free(da->mutexSum);
	free(da->condV);
	free(da->condP);

	da->tabF 		= NULL;
	da->tabV 		= NULL;
	da->tabP 		= NULL;
	da->vSum		= NULL;
	da->pSum		= NULL;
	da->fParam 		= NULL;
	da->vParam		= NULL;
	da->pParam		= NULL;
	da->fThread		= NULL;
	da->vThreads	= NULL;
	da->pThreads	= NULL;
	da->mutexUI		= NULL;
	da->mutexTabF	= NULL;
	da->mutexTabP	= NULL;
	da->mutexTabV	= NULL;
	da->mutexSum	= NULL;
	da->condV		= NULL;
	da->condP		= NULL;

}

void initMutex(DynamicallyAllocated *da){
	pthread_mutex_init(da->mutexUI, NULL);
	pthread_mutex_init(da->mutexTabF, NULL);
	pthread_mutex_init(da->mutexTabV, NULL);
	pthread_mutex_init(da->mutexTabP, NULL);
	pthread_mutex_init(da->mutexSum, NULL);
	pthread_mutex_init(&mutexS, NULL);
}

void destroyMutex(DynamicallyAllocated *da){
	pthread_mutex_destroy(da->mutexUI);
	pthread_mutex_destroy(da->mutexTabF);
	pthread_mutex_destroy(da->mutexTabV);
	pthread_mutex_destroy(da->mutexTabP);
	pthread_mutex_destroy(da->mutexSum);
	pthread_mutex_destroy(&mutexS);
}

void initCond(DynamicallyAllocated *da){
	int i;

	/*	Initialisation conditions voitures	*/
	for(i = 0; i < TAILLE_COND_V; i++)
		pthread_cond_init(&(da->condV[i]), NULL);

	/*	Initialisation conditions piétons	*/
	for(i = 0; i < TAILLE_COND_P; i++)
		pthread_cond_init(&(da->condP[i]), NULL);
}

void destroyCond(DynamicallyAllocated *da){
	int i;

	for(i = 0; i < TAILLE_COND_V; i++){
		if(pthread_cond_destroy(&(da->condV[i])) != 0)
			perror("PTHREAD_COND_DESTROY");
	}

	for(i = 0; i < TAILLE_COND_P; i++){
		if(pthread_cond_destroy(&(da->condP[i])) != 0)
			perror("PTHREAD_COND_DESTROY");
	}
}

void initParam(DynamicallyAllocated *da, Config config){
	int i;

	/*	Initialisation des paramètres du feux		*/
	da->fParam->tabF 		= da->tabF;
	da->fParam->tabV		= da->tabV;
	da->fParam->conf 		= config;
	da->fParam->mutexUI		= da->mutexUI;
	da->fParam->mutexTabF	= da->mutexTabF;
	da->fParam->mutexTabV	= da->mutexTabV;
	da->fParam->condV		= da->condV;
	da->fParam->condP		= da->condP;
	da->fParam->printFeux 	= printFeux_th;
	da->fParam->printTxt	= printTxt_th;
	da->fParam->printErr	= printErr_th;

	/*	Initialisation des paramètres de la voiture	*/

	for(i = 0; i < TAILLE_TAB_PARAM_V; i++){
		da->vParam[i].tabF 			= da->tabF;
		da->vParam[i].tabP 			= da->tabP;
		da->vParam[i].tabV			= da->tabV;
		da->vParam[i].sumV			= da->vSum;
		da->vParam[i].sumP			= da->pSum;
		da->vParam[i].emplacement	= i;
		da->vParam[i].conf			= config;
		da->vParam[i].condV			= da->condV;
		da->vParam[i].condP			= da->condP;
		da->vParam[i].mutexUI		= da->mutexUI;
		da->vParam[i].mutexTabF		= da->mutexTabF;
		da->vParam[i].mutexTabP		= da->mutexTabP;
		da->vParam[i].mutexTabV		= da->mutexTabV;
		da->vParam[i].mutexSum		= da->mutexSum;
		da->vParam[i].threads		= da->vThreads;
		da->vParam[i].threadFeux	= da->fThread;
		da->vParam[i].printFunc		= printV_th;
		da->vParam[i].printTxt		= printTxt_th;
		da->vParam[i].printErr		= printErr_th;
	}

	/*	Initialisation des paramètres du piéton		*/

	for(i = 0; i < TAILLE_TAB_PARAM_P; i++){
		da->pParam[i].tabF 			= da->tabF;
		da->pParam[i].tabP 			= da->tabP;
		da->pParam[i].tabV			= da->tabV;
		da->pParam[i].sumV			= da->vSum;
		da->pParam[i].sumP			= da->pSum;
		da->pParam[i].emplacement	= i;
		da->pParam[i].conf			= config;
		da->pParam[i].condV			= da->condV;
		da->pParam[i].condP			= da->condP;
		da->pParam[i].mutexUI		= da->mutexUI;
		da->pParam[i].mutexTabF		= da->mutexTabF;
		da->pParam[i].mutexTabP		= da->mutexTabP;
		da->pParam[i].mutexTabV		= da->mutexTabV;
		da->pParam[i].mutexSum		= da->mutexSum;
		da->pParam[i].threads		= da->pThreads;
		da->pParam[i].threadFeux	= da->fThread;
		da->pParam[i].printFunc		= printP_th;
		da->pParam[i].printTxt		= printTxt_th;
		da->pParam[i].printErr		= printErr_th;
	}
}

void erreurCarrefour(const char *msg, DynamicallyAllocated *da){
	freeMemCarrefour(da);
	endUI();

	perror(msg);
}

bool testAlloc(DynamicallyAllocated *da){
	bool a = 
		da->tabF 		== NULL ||
		da->tabV 		== NULL ||
		da->tabP 		== NULL ||
		da->vSum		== NULL ||
		da->pSum		== NULL ||
		da->fParam 		== NULL ||
		da->vParam		== NULL ||
		da->pParam		== NULL ||
		da->fThread		== NULL ||
		da->vThreads	== NULL ||
		da->pThreads	== NULL ||
		da->mutexUI		== NULL ||
		da->mutexTabF	== NULL ||
		da->mutexTabP	== NULL ||
		da->mutexTabV	== NULL ||
		da->mutexSum	== NULL ||
		da->condV		== NULL ||
		da->condP		== NULL;

	return !a;
}

bool setSignals(){
	struct sigaction sigINT, sigWINCH, sigUSR1, sigUSR2;

	sigINT.sa_flags		= 0;
	sigWINCH.sa_flags	= 0;
	sigUSR1.sa_flags	= 0;
	sigUSR2.sa_flags	= 0;

	sigemptyset(&(sigINT.sa_mask));
	sigemptyset(&(sigWINCH.sa_mask));
	sigemptyset(&(sigUSR1.sa_mask));
	sigemptyset(&(sigUSR2.sa_mask));

	sigINT.sa_handler = handlerSIGINT;
	sigWINCH.sa_handler = handlerSIGWINCH;
	sigUSR1.sa_handler = handlerSIGUSR1;
	sigUSR2.sa_handler = handlerSIGUSR2;

	if(
		sigaction(SIGINT, &sigINT, NULL) 		!= 0 ||
		sigaction(SIGWINCH, &sigWINCH, NULL)	!= 0 ||
		sigaction(SIGUSR1, &sigUSR1, NULL)		!= 0 ||
		sigaction(SIGUSR2, &sigUSR2, NULL)		!= 0
	){
		return false;
	}

	return true;
}

bool unSetSignals(){
	struct sigaction sigINT, sigWINCH;

	sigINT.sa_flags		= 0;
	sigWINCH.sa_flags	= 0;

	sigemptyset(&(sigINT.sa_mask));
	sigemptyset(&(sigWINCH.sa_mask));

	sigINT.sa_handler	= SIG_DFL;
	sigWINCH.sa_handler = SIG_DFL;

	if(
		sigaction(SIGINT, &sigINT, NULL) 		!= 0 ||
		sigaction(SIGWINCH, &sigWINCH, NULL)	!= 0
	){
		return false;
	}

	return true;
}
