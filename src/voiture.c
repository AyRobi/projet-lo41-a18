#include "voiture.h"

bool voitureTraverse[TAILLE_TAB_VOITURE_TRAVERSE];	/*	Variable utilisée pour savoir si une voiture est en train de traversé dans la file */

void *voiture(void *data){

	/*	récupération de la structure passée en paramètre	*/
	VP_Param *param = (VP_Param *) data;

	/*	On pose les masques de signaux	*/
	setMaskVP(param->printErr, param->mutexUI);

	/*	La voiture s'insère dans la file d'attente	*/
	arriverV(param);

	/*	On attend au feu	*/
	attendreAuFeuV(param);
	
	/*	La voiture démarre	*/
	demarrerV(param);

	/*	La voiture traverse	*/
	traverserV(param);

	/*	La voiture sort du carrefour	*/
	quitterV(param);

	pthread_exit((void *) EXIT_SUCCESS);
}

void arriverV(VP_Param *param){
	/*	On incremente de 1 le nombre de voiture dans la file	*/
	pthread_mutex_lock(param->mutexTabV);
	param->tabV[param->emplacement]++;
	param->printFunc(param->tabV, param->mutexUI);
	pthread_mutex_unlock(param->mutexTabV);

}

void attendreAuFeuV(VP_Param *param){
	
	/*	On regarde si on peut passer, si ce n'est pas le cas on s'endort	*/
	pthread_mutex_lock(param->mutexTabV);
	pthread_mutex_lock(param->mutexTabF);
	pthread_mutex_lock(param->mutexTabP);

	while(
		!(param->tabF[param->emplacement] == F_VERT)						||	/*	le feu n'est pas au vert					*/
		param->tabV[param->emplacement + V_H_T] >= param->conf.nbMaxVSurC	||	/*	Le carrefour est déja plein					*/
		voitureTraverse[param->emplacement]									||	/*	Une voiture est déjà en train de traverser	*/
		param->tabV[voitureGauche(param->emplacement + V_H_T)]				||	/*	Il y a une voiture sur le carrefour à gauche*/
		voitureTraverse[(param->emplacement + 1) % NB_FILE_V]				||	/*	Il y a une voiture qui démarre à gauche		*/
		param->tabP[pietonDevant(param->emplacement, true)]					||	/*	Un piéton traverse							*/
		param->tabP[pietonDevant(param->emplacement, false)]						/*	Un piéton traverse							*/
	){
			pthread_mutex_unlock(param->mutexTabP);
			pthread_mutex_unlock(param->mutexTabF);

			pthread_cond_wait(&(param->condV[param->emplacement]), param->mutexTabV);

			pthread_mutex_lock(param->mutexTabF);
			pthread_mutex_lock(param->mutexTabP);
		}
	pthread_mutex_unlock(param->mutexTabP);
	pthread_mutex_unlock(param->mutexTabF);
	pthread_mutex_unlock(param->mutexTabV);
}

void demarrerV(VP_Param *param){
	pthread_mutex_lock(param->mutexTabV);
	voitureTraverse[param->emplacement] = true;
	pthread_mutex_unlock(param->mutexTabV);

	usleep(param->conf.tpsDemarrageV * 1000);

	/*	La voiture entre dans la file du carrefour	*/
	pthread_mutex_lock(param->mutexTabV);
	voitureTraverse[param->emplacement] = false;
	pthread_mutex_unlock(param->mutexTabV);

}

void traverserV(VP_Param *param){

	bool stoppee = false;

	pthread_mutex_lock(param->mutexTabV);
	param->tabV[param->emplacement]--;				/*	On quitte la file du feu			*/
	param->tabV[param->emplacement + V_H_T]++;		/*	On entre dans la file du carrefour	*/
	param->printFunc(param->tabV, param->mutexUI);

	/*	Si il y a une voiture derrère moi je la reveille	*/
	if(param->tabV[param->emplacement])
		pthread_cond_signal(&(param->condV[param->emplacement]));

	pthread_mutex_unlock(param->mutexTabV);

	/*	La voiture traverse le carrefour	*/
	usleep(param->conf.tpsTraverseV * 1000);

	pthread_mutex_lock(param->mutexTabV);
	pthread_mutex_lock(param->mutexTabP);
	
	/*	Si on ne peut pas traversé sans tué quelqu'un on s'endort au volant...	*/
	while(
		param->tabV[voitureDevant(param->emplacement + V_H_T)]			||	/*	Une voiture traverse devant moi				*/
		param->tabP[pietonDevant(param->emplacement + V_H_T, true)]		||	/*	Un piéton est encore en train de traverser	*/
		param->tabP[pietonDevant(param->emplacement + V_H_T, false)]	||	/*	Un piéton est encore en train de traverser	*/
		voitureTraverse[param->emplacement + V_H_T]							/*	Si une voiture est en train de re-demmarer	*/
	){
		pthread_mutex_unlock(param->mutexTabP);

		/*	On passe l'état de la voiture à stoppée	*/
		stoppee = true;

		/*	On dort	*/
		pthread_cond_wait(&(param->condV[param->emplacement + V_H_T]), param->mutexTabV);

		pthread_mutex_lock(param->mutexTabP);
	}
	pthread_mutex_unlock(param->mutexTabP);

	/*	Si on a été stoppé la file re-démarre voiture par voiture	*/
	if(stoppee){
		voitureTraverse[param->emplacement + V_H_T] = true;
		pthread_mutex_unlock(param->mutexTabV);

		/*	On démarre	*/
		usleep(param->conf.tpsDemarrageV * 1000);

		pthread_mutex_lock(param->mutexTabV);
		voitureTraverse[param->emplacement + V_H_T] = false;
	}

	/*	Si il y a une voiture à ma gauche je la reveille	*/
	if(param->tabV[voitureGauche(param->emplacement + V_H_T)])
		pthread_cond_signal(&(param->condV[voitureGauche(param->emplacement + V_H_T)]));

	/*	Je quitte le carrefour	*/
	param->tabV[param->emplacement + V_H_T]--;
	param->printFunc(param->tabV, param->mutexUI);

	/*	Je reveille la voiture derrière moi	*/
	if(param->tabV[param->emplacement + V_H_T])
		pthread_cond_signal(&(param->condV[param->emplacement + V_H_T]));

	/*	Si il y a une voiture au feu je la reveille	*/
	if(param->tabV[param->emplacement])
		pthread_cond_signal(&(param->condV[param->emplacement]));

	pthread_mutex_unlock(param->mutexTabV);

}

void quitterV(VP_Param *param){
	
	/*	Avant de quitter on met à jour les totaux et la liste de voiture	*/
	pthread_mutex_lock(param->mutexSum);

	/*	On supprime la voiture de la liste des threads	*/
	rmThreadsFromTab(param->threads, *(param->sumV), pthread_self());

	/*	On décremante le nombre de voitures total et on affiche la nouvelle valeur	*/
	*(param->sumV) = *(param->sumV) - 1;
	printSum(param->printTxt, param->mutexUI, *(param->sumV), *(param->sumP));

	pthread_mutex_unlock(param->mutexSum);

}

int voitureDevant(int vNum){
	
	int num;
	switch (vNum)
	{
		case V_H_T:
			num = V_G_T;
			break;
		case V_G_T:
			num = V_B_T;
			break;
		case V_D_T:
			num = V_H_T;
			break;
		case V_B_T:
			num = V_D_T;
			break;
		default:
			num = 0;
			break;
	}

	return num;
}

int voitureGauche(int vNum){
	
	int num;
	switch (vNum)
	{
		case V_H_T:
			num = V_D_T;
			break;
		case V_G_T:
			num = V_H_T;
			break;
		case V_D_T:
			num = V_B_T;
			break;
		case V_B_T:
			num = V_G_T;
			break;
		default:
			num = 0;
			break;
	}

	return num;
}

void initVoitureMem(){
	unsigned int i;

	for(i = 0; i < TAILLE_TAB_VOITURE_TRAVERSE; i++)
		voitureTraverse[i] = false;
}

unsigned int pietonDevant(unsigned int v, bool top){
	unsigned int p = 0;

	if(top){
		
		switch (v)
		{
			case V_H:
				p = P_H_GD;
				break;

			case V_D:
				p = P_D_HB;
				break;

			case V_B:
				p = P_B_GD;
				break;

			case V_G:
				p = P_G_HB;
				break;
			
			case V_H_T:
				p = P_B_GD;
				break;

			case V_D_T:
				p = P_G_HB;
				break;

			case V_B_T:
				p = P_H_GD;
				break;

			case V_G_T:
				p = P_D_HB;
				break;

			default:
				break;
		}
	}else{
		switch (v)
		{
			case V_H:
				p = P_H_DG;
				break;

			case V_D:
				p = P_D_BH;
				break;

			case V_B:
				p = P_B_DG;
				break;

			case V_G:
				p = P_G_BH;
				break;
			
			case V_H_T:
				p = P_B_DG;
				break;

			case V_D_T:
				p = P_G_BH;
				break;

			case V_B_T:
				p = P_H_DG;
				break;

			case V_G_T:
				p = P_D_BH;
				break;

			default:
				break;
		}
	}

	return p;
}