#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "carrefour.h"
#include "config.h"
#include "menu.h"

int main(int argc, char ** argv){
    srand(time(NULL));
    Config config;
    int choix, error = EXIT_SUCCESS;
    chargement_config(&config);
    choix = affiche_menu();
    while (choix != 0 && error == EXIT_SUCCESS)
    {
        switch (choix) 
        {   case 1 : error = startCarrefour(config); break;
            case 2 : gestion_sous_menu(&config);
		     break;
	    case 3 :affichage_configuration(&config); break;
            default : printf("erreur de choix "); break;
        }
        choix = affiche_menu();
    }
      sauvegarde_config(&config);
      return EXIT_SUCCESS;
  
}
