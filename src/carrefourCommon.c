#include "carrefourCommon.h"

void printSum(void (*printFunc)(const char *, pthread_mutex_t *), pthread_mutex_t *mutex, int vSum, int pSum){

	char strTemp[TAILLE_TEMP_STR];

	snprintf(
		strTemp,
		TAILLE_TEMP_STR,
		"Nombre total de voitures : %d\n"
		"Nombre total de piétons  : %d",
		vSum,
		pSum
		);

	printFunc(strTemp, mutex);	/*	Affichage des nouvelles valeurs de sommes	*/
}

void handlerSIGUSR1(int a){
	pthread_mutex_lock(&mutexS);

	if(COUNT_DEAD != -1){
		DEAD_T[COUNT_DEAD] = pthread_self();
		COUNT_DEAD++;
	}
	pthread_mutex_unlock(&mutexS);

	pthread_exit((void *) EXIT_SUCCESS);
}

void setMaskVP(void (*printErr)(const char *, pthread_mutex_t *), pthread_mutex_t *mutexUI){
	sigset_t newSigSet;

	/*	on bloque SIGINT, SIGWINCH et SIGUSR2	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGINT);
	sigaddset(&newSigSet, SIGWINCH);
	sigaddset(&newSigSet, SIGUSR2);
	if(pthread_sigmask(SIG_BLOCK, &newSigSet, NULL) == -1){
		printErr("SIGPROCMASK", mutexUI);
		pthread_exit((void *) EXIT_FAILURE);
	}

	/*	on autorise SIGUSR1	*/
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGUSR1);
	if(pthread_sigmask(SIG_UNBLOCK, &newSigSet, NULL) == -1){
		printErr("SIGPROCMASK", mutexUI);
		pthread_exit((void *) EXIT_FAILURE);
	}
}

int rmThreadsFromTab(pthread_t threads[], const int tabSize, pthread_t thisThread){
	int i = 0;

	while(i < tabSize && threads[i] != thisThread)
		i++;

	while(i < tabSize - 1){
		threads[i] = threads[i + 1];
		i++;
	}

	if(i == tabSize)
		return -1;
	else
		return 0;
}