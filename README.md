# Projet UV LO41 automne 2018 : Gestion d'un carrefour routier

Ce dépôt contient les sources d'un projet utilisant massivement les primitives disponibles sous un système basé sur le noyau Linux. En effet, il vise à synchroniser un ensemble de véhicules et de piétons, tous représentés par un thread distinct, dans l'objectif de traverser un carrefour à feux sans collision.

## Compilation
Il est nécessaire de compiler le programme sur votre propre machine pour l'utiliser dans les meilleures conditions possibles, pour cela suivez les étapes suivantes :
1. Depuis le dossier racine (où il y a *Makefile*) exécuter la commande
```
make
```
Si la **commande réussis** passer à la partie *exécution* **sinon** continuer
1. Dans le cas où ```make``` échoue vous devez installer la librairie *ncurses* avec la commande 
```
sudo apt-get install libncurses5-dev libncursesw5-dev
```
2. Faites de nouveau
```
make
```

## Exécution
Une fois le programme compilé, exécuté le avec (toujours depuis le dossier du README.md)
```
./bin/app
```

*NB: Il est possible qu'un message d'erreur vous indiquant que le fichier de configuration n'est pas trouvé, dans ce cas une configuration par défaut est chargé. Cela n'empêche donc pas le bon fonctionnement du programme. De plus le fichier seras créé à la fermeture du logiciel.*

## Utilisation
### Menu principal
Le menu principal propose de :
1. Lancer la simulation du carrefour
1. Modifier les paramètres utilisé par le carrefour pour influencer la simulation
1. Voir l'état de la configuration actuellement utilisée par la simulation

## Simulation du carrefour
La simulation du carrefour dessine une fenêtre semblable à celle-ci :

![ScreenShot simulation carrefour](doc/img/screenShot.png)

Vous pouvez donc remarquer que le carrefour à 4 voies apparaît clairement ainsi que les feux pour les voitures et les piétons.
Les numéros correspondent au nombre de voitures/piétons dans une file donnée.
Enfin, la partie base de l'écran est réservée aux messages informatifs (ex : nombre total de voiture/piéton ; prise en compte d'un appel piéton, ...) et aux messages d'erreurs.

*Attention : il est possible que vous ne voyiez pas le carrefour dans sa globalité, dans ce cas il faut changer les paramètres de la console pour avoir un minimum de 45 lignes et 84 colonnes. Ensuite, relancer le programme.*

### Stopper la simulation
La simulation se stop avec un
```
Ctrl-C
```
Vous serez ensuite redirigé vers le menu principal.

### Options de simulation
* **Temps de phase (s)** : Temps que met le feu à faire un cycle complet (vert puis orange puis ronge puis vert)
* **Temps d’interphase (s)** : Temps du feu orange. Doit être inférieur au temps de phase.
* **Taille de file critique** : Taille minimal d'une file de voiture à atteindre pour accorder un feu vert plus long.
* **Temps additionnel (s)** : Temps supplémentaire accordé à une file de taille critique.
* **Temps de démarrage voiture (ms)** : Temps que met une voiture pour démarrer.
* **Temps traversé voiture (ms)** : Temps que met une voiture à traverser le carrefour (hors interruption et temps de démarrage).
* **Nombre maximal de voitures sur le carrefour** : Définit le nombre maximal de voitures traversant le carrefour par file.
* **Débit voiture moyen (V/h)** : Donne le nombre moyen de voitures qui arrivent au carrefour en une heure.
* **Répartition des voitures [4]** : Donne pour chaque branche le taux de fréquentation en pourcent par rapport à la moyenne. La somme de tous les taux doit faire 100.
* **Débit moyen piéton (P/h)** : Donne le nombre moyen de piéton qui traversent le carrefour en une heure.
* **Répartition des piétons [8]** : Permet de moduler la répartition des piétons dans le carrefour.
* **Temps traversé piéton (s)** : Temps que met un piéton pour traverser la route.
* **Probabilité bouton** : Définit la probabilité que le piéton appuis sur le bouton et provoque ainsi un changement de phase.

### Auteurs
* BIRLING Lucas (lucas)
* ROBITAILLE Aymeric (AyRobi)
